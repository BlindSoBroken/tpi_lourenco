<?php
/**
 * Created by PhpStorm.
 * User: Bruno.DA-COSTA-LOURE
 * Date: 14.05.2019
 * Time: 09:08
 */
?>
<header>
    <nav class="navbar navbar-expand-lg navbar-dark pb_navbar pb_scrolled-light" id="templateux-navbar">
        <div class="container">
            <a class="navbar-brand" href="index.php?action=view_home"><span class="text-danger">Auto</span>Swiss<span class="text-danger">Rent</span></a>
            <div class="site-menu-toggle js-site-menu-toggle  ml-auto"  data-aos="fade" data-toggle="collapse" data-target="#templateux-navbar-nav" aria-controls="templateux-navbar-nav" aria-expanded="false" aria-label="Toggle navigation">
                <span></span>
                <span></span>
                <span></span>
            </div>
            <!-- END menu-toggle -->

            <div class="collapse navbar-collapse" id="templateux-navbar-nav">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item"><a class="nav-link" href="index.php?action=view_home">Accueil</a></li>
                    <?php
                    if (isset($_SESSION['login']))
                    {
                        if ($_SESSION['categoryUser']==1)
                        {
                            ?>
                            <li class="nav-item"><a class="nav-link" href="index.php?action=view_listVehicles">Listes des véhicules</a></li>
                            <li class="nav-item"><a class="nav-link" href="index.php?action=view_profile">Mon Profil</a></li>
                            <li class="nav-item"><a class="nav-link" href="index.php?action=view_mybookings&uID=<?=$_SESSION['uID']?>">Mes réservations</a></li>
                            <li class="nav-item cta-btn ml-xl-2 ml-lg-2 ml-md-0 ml-sm-0 ml-0"><a class="nav-link" href="index.php?action=view_logout" target="_parent"><span class="pb_rounded-4 px-4 rounded">Deconnexion</span></a></li>
                            <?php
                        }
                        elseif ($_SESSION['categoryUser']==0)
                        {
                            ?>
                            <li class="nav-item"><a class="nav-link" href="index.php?action=view_vehiclesManagement">Gestion véhicules</a> </li>
                            <li class="nav-item"><a class="nav-link" href="index.php?action=view_clientsManagement">Gestion clients</a> </li>
                            <li class="nav-item cta-btn ml-xl-2 ml-lg-2 ml-md-0 ml-sm-0 ml-0"><a class="nav-link" href="index.php?action=view_logout" target="_parent"><span class="pb_rounded-4 px-4 rounded">Deconnexion</span></a></li>
                            <?php
                        }
                    }
                    else
                    {
                        ?>
                        <li class="nav-item"><a class="nav-link" href="#section-about">A propos</a></li>
                        <li class="nav-item"><a class="nav-link" href="#section-team">Contact</a></li>
                        <li class="nav-item cta-btn ml-xl-2 ml-lg-2 ml-md-0 ml-sm-0 ml-0"><a class="nav-link" href="index.php?action=view_login" target="_parent"><span class="pb_rounded-4 px-4 rounded">Se connecter</span></a></li>
                        <?php
                    }
                    ?>
                </ul>
            </div>
        </div>
    </nav>
</header>

