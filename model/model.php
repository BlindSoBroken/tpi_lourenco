<?php
/**
 * Created by PhpStorm.
 * User: Bruno.DA-COSTA-LOURE
 * Date: 15.05.2019
 * Time: 08:20
 */

/***
 * @return PDO
 * This function is used to connect to the database
 */
function getDB()
{
    $DB = new PDO('mysql:host=localhost:3306;dbname=autoswissrent;charset=utf8', 'root', '');
    //$DB = new PDO('mysql:host=localhost;dbname=bdacosta_autoCH', 'bdacosta_autoCH', 'EgAxw9B6exy9Scb$');
    $DB->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    return $DB;
}

/***
 * @param $registerValues
 * @return array
 * @throws Exception : If the user give an information that is not valid by the function then we return an error message in the form prefilled
 * This function is used to get the form from the user and check his data and insert in the database if is is correct.
 */
function sign_in($registerValues) //Inscription
{
    //Connexion à la base de donnée
    $bdd = getDB();

    //Recupération des POST et les transférer dans des variables depuis la méthode "Extract"
    extract($registerValues);

    $login=$inscriptionUsername;
    $prenom=$inscriptionPrenom;
    $nom=$inscriptionNom;
    $adresse=$inscriptionAdresse;
    $NPA=$inscriptionNPA;
    $ville=$inscriptionVille;
    $mail=$inscriptionMail;
    $password=$inscriptionPassword;
    $password2=$inscriptionPassword2;
    #$paiementMethod=$inscriptionPaiementMethod;

    //Algorithme de cryptage pour le mot de passe
    $algo = PASSWORD_DEFAULT;

    //Vérification du contenu des données envoyée par l'utilisateur (si elle sont vide)
    if($login!="" && $prenom!="" && $nom!="" && $adresse!="" && $NPA!="" && $ville!="" && $mail!="" && $password!="" && $password2!="")
    {
        //Relève le nombre de caractère du code postal dans une variable
        $NPA_length= strlen($NPA);
        //Vérification si le code postal contient bien 4 chiffres commançant par 1
        if (preg_match('`[1-9]{1}[0-9]{3}`',$NPA) && $NPA_length == 4)
        {
            //Vérification si l'adresse mail est valide
            if (preg_match('#^[\w.-]+@[\w.-]+\.[a-z]{2,6}$#i',$mail))
            {
                //Vérification que le mot de passe contient bien une majuscule, une minuscule et un chiffre au moins
                if (preg_match('#^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*\W)#', $password))
                {
                    //Vérification si les deux mot de passe entré par l'utilisateur correspondent bien
                    if ($password == $password2) {

                        //hash le mot de passe
                        $mdp = password_hash( $password, $algo);

                        //Préparation et execution de la requête qui va récupérer certaines données dans la base de données afin d'éffectuer des vérifications
                        $queryMail = $bdd->prepare('SELECT `mailAddressUser`, `loginUser` FROM `users` WHERE `mailAddressUser` ="'. $mail.'"');
                        $queryMail->execute();
                        $resultats = $queryMail->fetch(PDO::FETCH_ASSOC);
                        $queryMail->closeCursor();

                        //Vérification que l'adresse mail n'existe pas déjà dans la base de donnée
                        if (strtolower($mail) != strtolower($resultats['mailAddressUser']))
                        {

                            //Vérification que l'identifiant de l'utilisateur n'existe pas déjà dans la base de donnée
                            if (strtolower($login) != strtolower($resultats['loginUser']))
                            {
                                //Requête pour insérer les données dans la base de données
                                $query = $bdd->prepare('INSERT INTO `Users` (`firstNameUser`, `lastNameUser`, `addressUser`, `npaUser`, `cityUser`, `mailAddressUser`, `loginUser`, `passwordUser`, fkcategoryUser) VALUES
                            ("' . $prenom . '", "' . $nom . '", "' . $adresse . '", "' . $NPA . '", "' . $ville . '", "' . $mail . '", "' . $login . '","' . $mdp . '","' . 1 . '")');

                                $query->execute();
                                $tableau = array(
                                    "mailAddressUser" => $mail,
                                    "loginUser" => $login,
                                    "fkcategoryUser" => 1
                                );
                                return $tableau;
                            }
                            else
                            {
                                $error = '7';
                                throw new Exception($error);
                            }
                        }
                        else
                        {
                            $error = '6';
                            throw new Exception($error);
                        }
                    }
                    else
                    {
                        $error = '5';
                        throw new Exception($error);
                    }
                }
                else
                {
                    $error = '4';
                    throw new Exception($error);
                }
            }
            else
            {
                $error = '3';
                throw new Exception($error);
            }
        }
        else
        {
            $error = '2';
            throw new Exception($error);
        }

    }
    else
    {
        $error = '1';
        throw new Exception($error);
    }
}

/***
 * @param $connexionValues
 * @return mixed
 * @throws Exception : If the user's credentials aren't validated by the function then we return an error message in the empty form
 * This function is used to check the credentials sent by an user and compare it to the database's ones. Log in the user if it succeeds
 */
function log_in($connexionValues)
{
    //Recupération des POST et les transférer dans des variables depuis la méthode "Extract"
    extract($connexionValues);

    $identifiants =$connexionIdentifiant;
    $mdp =$connexionPassword;

    //Connexion à la base de donnée
    $bdd = getDB();

    //Préparation et execution de la requête qui va récupérer les données de l'utilisateur
    $query = $bdd->prepare('SELECT idUser,loginUser, passwordUser, mailAddressUser, fkcategoryUser FROM users WHERE mailAddressUser="'.$identifiants.'" OR loginUser="'.$identifiants.'"');
    $query->execute();
    $resultquery=$query->fetch(PDO::FETCH_ASSOC);
    $query->closeCursor();

    //Vérification du résultat de la requête
    if($resultquery!="")
    {
        //Comparaison du mot de passe entré par l'utilisateur et celui de la base de donnée
        if(password_verify($mdp, $resultquery['passwordUser']))
        {
            //Création d'un tableau avec les données de l'utilisateur et on le retourne au contrôleur
            $tableau=array(
                "mailAddressUser" => $resultquery['mailAddressUser'],
                "loginUser" =>$resultquery['loginUser'],
                "fkcategoryUser" =>$resultquery['fkcategoryUser'],
                "idUser" =>$resultquery['idUser']
            );
            return $resultquery;
        }
    }
    $error = 'msg';
    throw new Exception($error);

}

/***
 * @param $user
 * @return mixed
 * @throws Exception : If the user's id doesn't exists in the database then throws an exception
 * This function is used to get the personal information of the user
 */
function getProfileData($user)
{
    //Connexion à la base de données
    $bdd = getDB();

    //Préparation et execution de la requête qui va recupérer les données de l'utilisateur
    $query = $bdd->prepare("SELECT idUser,firstNameUser,lastNameUser,addressUser,cityUser,npaUser,mailAddressUser FROM users WHERE loginUser='".$user."'");
    $query->execute();
    $resultquery = $query->fetch();

    //Vérification du résultat de la requête
    if ($resultquery['idUser'] == "")
    {
        $erreur = true;
        throw new Exception($erreur);
    }
    else
    {
        return $resultquery;
    }
}

/***
 * @param $updateData
 * @param $user
 * @throws Exception : If the informations sent by the user aren't valid then throws an exception.
 * This function is used to update the password of an user.
 */
function updatePWD($updateData, $user)
{
    //Vérification si l'utilisateur est un client ou un administrateur
    if ($_SESSION['categoryUser']==1)
    {
        //Recupération des POST et les transférer dans des variables depuis la méthode "Extract"
        extract($updateData);

        $old=$profilOldPassword;
        $new=$profilNewPassword;
        $newConfirm=$profilNewPassword2;

        //Algorithme de cryptage pour le mot de passe
        $algo = PASSWORD_DEFAULT;

        //Connexion à la base de données
        $bdd = getDB();

        //Préparation et execution de la requête qui va recupérer le mot de passe de l'utilisateur
        $query = $bdd->prepare("SELECT passwordUser FROM users WHERE idUser='".$user."'");
        $query->execute();
        $resultquery = $query->fetch();

        //Vérification des deux mots de passe envoyé par l'utilisateur
        if($new == $newConfirm)
        {
            //hash le mot de passe
            $mdp = password_hash($new, $algo);

            //Vérification de l'ancien mot de passe retourné par la base de données avec celui entré par l'utilisateur
            if (password_verify($old,$resultquery['passwordUser']))
            {
                //Préparation et execution de la requête qui va mettre à jour le mot de passe de l'utilisateur
                $query2 = $bdd->prepare("UPDATE users SET passwordUser='".$mdp."' WHERE idUser='".$user."'");
                $query2->execute();
            }
            else
            {
                $error = '1';
                throw new Exception($error);
            }
        }
        else
        {
            $error = '2';
            throw new Exception($error);
        }
    }
    else
    {
        //Recupération des POST et les transférer dans des variables depuis la méthode "Extract"
        extract($updateData);

        $new=$newPassword;
        $newConfirm=$newPassword2;

        //Algorithme de cryptage pour le mot de passe
        $algo = PASSWORD_DEFAULT;

        //Connexion à la base de données
        $bdd = getDB();

        //Vérification des deux mots de passe envoyé par l'utilisateur
        if($new == $newConfirm)
        {
            //hash le mot de passe
            $mdp = password_hash($new, $algo);

            //Préparation et execution de la requête qui va mettre à jour le mot de passe de l'utilisateur
            $query2 = $bdd->prepare("UPDATE users SET passwordUser='".$mdp."' WHERE idUser='".$user."'");
            $query2->execute();
        }
        else
        {
            $erreur = '1';
            throw new Exception($erreur);
        }

    }
}

/***
 * @return array
 * This function is used to get the informations of all the clients
 */
function getClients()
{
    //Connexion à la base de données
    $bdd = getDB();

    //Préparation et execution de la requête qui va récupérer tous les utilisateurs
    $query = $bdd->prepare("SELECT * FROM users");
    $query->execute();

    $array1=$query->fetchAll();

    //Déclaration de la variable qui va sérvir de compteur dans la boucle
    $i=0;

    foreach($array1 as $array)
    {
        $firstname=$array1[$i]['firstNameUser'];

        //Préparation et execution de la requête qui va compter le nombre de réservation par client
        $query2 = $bdd->prepare("SELECT COUNT(*) as `total` FROM users LEFT JOIN booking ON users.idUser=booking.fkusers WHERE firstNameUser='".$firstname."'AND idBooking IS NOT NULL");
        $query2->execute();

        //On place dans un tableau temporaire le résultat
        $nbLocation=$query2->fetch();

        //On ajoute la valeur dans le tableau principale pour chaque client
        array_push($array1[$i],$array1[$i]["totalLocation"]=$nbLocation['total']);
        $i++;
    }

    //On retourne le tableau
    return $array1;
}

/***
 * @return array
 * This function is used to get the informations of all vehicles
 */
function getVehicles()
{
    //Connexion à la base de données
    $bdd = getDB();

    //Préparation et execution de la requête qui va récupérer tous les véhicules
    $query = $bdd->prepare("SELECT * FROM vehicles");
    $query->execute();

    //Place les informations dans un tableau et les retournes
    return $query->fetchAll();
}

/***
 * @param $vID
 * @return mixed
 * This function is used to get the informations of a spécific véhicle
 */
function getVehiclesSingle($vID)
{
    //Connexion de la base de données
    $bdd = getDB();

    //Préparation et execution de la requête  qui va récupérer les informations d'un véhicule
    $query = $bdd->prepare('SELECT * FROM vehicles WHERE idVehicle='.$vID);
    $query->execute();

    //Place les informations dans un tableau et les retournes
    return $query->fetch();
}

/***
 * @param $data
 * @return array
 * This function is used to get the informations of all the véhicles that are available
 */
function getVehiclesAvailable($data)
{
    //Connexion de la base de données
    $bdd = getDB();

    //Préparation et execution de la requête qui va récupérer les informations des véhicules qui sont disponibles dans les dates mentionnées par l'utilisateur
    $query = $bdd->prepare('SELECT * FROM vehicles LEFT JOIN booking ON vehicles.idVehicle=booking.fkvehicles WHERE disponibility=1 AND bookingDateStart NOT BETWEEN "'.$data['dateStart'].'" AND "'.$data['dateEnd'].'" OR fkvehicles IS NULL');
    $query->execute();

    //Place les informations dans un tableau et les retournes
    return $query->fetchAll();
}

/***
 * @param $vID
 * @return mixed
 * This function is used to get the disponibility the vehicle
 */
function getVehicleDisponibility($vID)
{
    //Connexion de la base de données
    $bdd = getDB();

    //Préparation et execution de la requête qui va récupérer la disponibilité du véhicule mentionné par l'administrateur
    $query = $bdd->prepare('SELECT disponibility FROM vehicles WHERE idVehicle='.$vID);
    $query->execute();

    //Place les informations dans un tableau et les retournes
    return $query->fetch();
}

/***
 * @param $uID
 * This function is used to delete an user from the management panel
 */
function deleteClient($uID)
{
    //Connexion de la base de données
    $bdd = getDB();

    //Préparation et execution de la requête qui supprimé l'utilisateur mentionné par l'administrateur
    $query = $bdd->prepare('DELETE FROM users WHERE idUser='.$uID);
    $query->execute();
}

/***
 * @param $data
 * @throws Exception : If there is a problem with the upload of the image then throws an exception.
 * This function is used to add a vehicle
 */
function vehicleAdd($data)
{
    //Connexion à la base de données
    $bdd = getDB();

    //Recupération des POST et les transférer dans des variables depuis la méthode "Extract"
    extract($data);

    $brand=$brandVehicle;
    $model=$modelVehicle;
    $registrationNumber=$registrationNumberVehicle;
    $locationPrice=$locationPriceVehicle;
    $mileage=$mileageVehicle;
    $gearBox=$gearBoxVehicle;
    $doors=$doorsVehicle;
    $title=$brand." ".$model." ".$registrationNumber;

    //Vérification du contenu des champs
    if ($brand!="" && $model!="" && $registrationNumber!="" && $locationPrice!="" && $mileage!="" && $gearBox!="" && $doors!="")
    {
        //Vérification que le nombre de portes soit cohérent
        if ($doors == 5 || $doors == 3)
        {
            //Vérification que le prix de location soit bien un nombre
            if (is_float(intval($locationPrice)) || is_int(intval($locationPrice)))
            {
                $length = strlen($registrationNumber);

                //Vérification de la longueur du matricule
                if ($length == 9)
                {
                    //Vérification que le matricule est bien un nombre
                    if (is_int(intval($registrationNumber)))
                    {
                        //Vérification que le champs contient bien Automatique ou Manuel
                        if ($gearBox == "Manuel" || $gearBox == "Automatique" || $gearBox == "manuel" || $gearBox == "automatique" || $gearBox == "auto" || $gearBox == "Auto") {
                            $i = null;

                            //Traitement de l'image
                            $tmp_file = $_FILES['imgVehicle']['tmp_name'];
                            $fichier = basename($_FILES['imgVehicle']['name']);

                            //on récupère la taille de l'image
                            $tailleimg = filesize($tmp_file);

                            //on récupère l'extension de l'image
                            $extension = strrchr($_FILES['imgVehicle']['name'], '.');

                            //on définit la taille max de l'image
                            $taille_maxi = 2097152;//2mo

                            //On définit les extensions d'image acceptées
                            $extensions = array('.png', '.gif', '.jpg', '.jpeg');

                            //Recherche et remplace les charactères afin d'avoir un nom cohérent et qui ne pose pas de problème avec des charactères spéciaux
                            $title = preg_replace('/([^.a-z0-9]+)/i', '-', $title);

                            //Début des vérifications de sécurité...

                            //Si l'extension n'est pas dans le tableau
                            if (!in_array($extension, $extensions)) {
                                $i++;
                                $erreur = 'Vous devez uploader un fichier de type png, gif, jpg, jpeg';
                            } //Si la taille du fichier dépasse la taille maximale autorisée
                            elseif ($tailleimg > $taille_maxi || $tailleimg == FALSE) {
                                $i++;
                                $erreur = 'Le fichier est trop gros... il doit faire moins de 2mo!';
                            }

                            //Vérification de la variable qui ne doit pas contenir d'erreur
                            if (empty($erreur)) {

                                //On récupère le chemin d'accès jusqu'au répertoire de destination
                                $repertoireDestination = getcwd() . "/content/data/vehicles/" . $title . "/";

                                //Vérifie si le répértoire existe sinon le créer avec les droits pour l'éditez les fichiers à l'intérieur
                                if (!file_exists($repertoireDestination)) {
                                    mkdir($repertoireDestination, 0777);
                                }

                                //On définit un nom de fichier
                                $nomFichier = $title . $extension;

                                //On formate le nom du fichier ici
                                $fichier = strtr($nomFichier,
                                    'ÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÒÓÔÕÖÙÚÛÜÝàáâãäåçèéêëìíîïðòóôõöùúûüýÿ',
                                    'AAAAAACEEEEIIIIOOOOOUUUUYaaaaaaceeeeiiiioooooouuuuyy');

                                //On remplace l'ancien nom du fichier par le nouveau
                                $fichier = preg_replace('/([^.a-z0-9]+)/i', '-', $fichier);

                                $picture = $repertoireDestination . $fichier;
                                chmod($tmp_file, 0755);

                                //Si la fonction renvoie false, c'est que ça n'a pas fonctionné...
                                if (!rename($tmp_file, $picture)) {
                                    $i++;
                                    $erreur = "Echec de l'upload !";
                                }
                            } else {
                                throw new Exception($erreur);
                            }
                            $list = array(array("content/data/vehicles/" . $title . "/" . $fichier)
                            );

                            //Créer le fichier CSV et l'ouvre pour écrire dedans
                            $fp = fopen('content/data/vehicles/' . $title . '/' . $title . '.csv', 'a+');

                            //Met le chemin de l'image dans le fichier CSV
                            foreach ($list as $fields) {
                                fputcsv($fp, $fields);
                            }

                            fclose($fp);

                            $repertoireIMG = "/content/data/vehicles/" . $title . "/" . $fichier;

                            //Préparation et execution de la requête qui va insérer les informations du véhicules ainsi que le chemin de l'image
                            $query = $bdd->prepare('INSERT INTO vehicles (brand, model, registrationNumber, locationPricePerDay, mileage, gearBox, doors, picture) VALUES(:brand, :model, :registrationNumber, :locationPricePerDay, :mileage, :gearBox, :doors, :picture)');

                            $query->execute(array(
                                'brand' => $brand,
                                'model' => $model,
                                'registrationNumber' => $registrationNumber,
                                'locationPricePerDay' => $locationPrice,
                                'mileage' => $mileage,
                                'gearBox' => $gearBox,
                                'doors' => $doors,
                                'picture' => $repertoireIMG
                            ));
                        }
                        else
                        {
                            $erreur = "Le champs 'Boîte de vitesse' ne contient pas manuel ou automatique !";
                            throw new Exception($erreur);
                        }
                    }
                    else
                    {
                        $erreur = "Le format du matricule est incorrect !";
                        throw new Exception($erreur);
                    }
                }
                else
                {
                    $erreur = "Le n° de matricule est invalide !";
                    throw new Exception($erreur);
                }
            }
            else
            {
                $erreur = "Le prix doit être un nombre !";
                throw new Exception($erreur);
            }
        }
        else
        {
            $erreur = "Le nombre de portes est incohérent !";
            throw new Exception($erreur);
        }
    }
    else
    {
        $erreur = "Certains champs sont vides !";
        throw new Exception($erreur);
    }
}

/***
 * @param $vID
 * @param $data
 * @throws Exception : If there is a problem with the upload of the image then throws an exception.
 * This function is used to modify the informations of an existing vehicle
 */
function vehicleModify($vID, $data)
{
    //Connexion à la base de données
    $bdd = getDB();

    //Recupération des POST et les transférer dans des variables depuis la méthode "Extract"
    extract($data);

    $brand=$brandVehicle;
    $model=$modelVehicle;
    $registrationNumber=$registrationNumberVehicle;
    $locationPrice=$locationPriceVehicle;
    $mileage=$mileageVehicle;
    $gearBox=$gearBoxVehicle;
    $doors=$doorsVehicle;
    $title=$brand." ".$model." ".$registrationNumber;

    //Vérification du contenu des champs
    if ($brand!="" && $model!="" && $registrationNumber!="" && $locationPrice!="" && $mileage!="" && $gearBox!="" && $doors!="")
    {
        //Vérification que le nombre de portes soit cohérent
        if ($doors == 5 || $doors == 3)
        {
            //Vérification que le prix de location soit bien un nombre
            if (is_float(intval($locationPrice)) || is_int(intval($locationPrice)))
            {
                $length = strlen($registrationNumber);

                //Vérification de la longueur du matricule
                if ($length == 9)
                {
                    //Vérification que le matricule est bien un nombre
                    if (is_int(intval($registrationNumber)))
                    {
                        //Vérification que le champs contient bien Automatique ou Manuel
                        if ($gearBox == "Manuel" || $gearBox == "Automatique" || $gearBox == "manuel" || $gearBox == "automatique" || $gearBox == "auto" || $gearBox == "Auto")
                        {
                            //Vérification si l'image doit être changée
                            if ($_FILES['imgVehicle']['name'] == "")
                            {

                                //Préparation et execution de la requête qui va mettre à jour les informations du véhicule
                                $query = $bdd->prepare('UPDATE vehicles SET brand="' . $brand . '", model="' . $model . '", registrationNumber=' . $registrationNumber . ', locationPricePerDay=' . $locationPrice . ', mileage=' . $mileage . ', gearBox="' . $gearBox . '", doors=' . $doors . ' WHERE idVehicle='.$vID);
                                $query->execute();
                            } else {
                                $i = null;

                                //Traitement de l'image
                                $tmp_file = $_FILES['imgVehicle']['tmp_name'];
                                $fichier = basename($_FILES['imgVehicle']['name']);

                                //on récupère la taille de l'image
                                $tailleimg = filesize($tmp_file);

                                //on récupère l'extension de l'image
                                $extension = strrchr($_FILES['imgVehicle']['name'], '.');

                                //on définit la taille max de l'image
                                $taille_maxi = 2097152;//2mo

                                //On définit les extensions d'image acceptées
                                $extensions = array('.png', '.gif', '.jpg', '.jpeg');

                                //Recherche et remplace les charactères afin d'avoir un nom cohérent et qui ne pose pas de problème avec des charactères spéciaux
                                $title = preg_replace('/([^.a-z0-9]+)/i', '-', $title);

                                //Début des vérifications de sécurité...

                                //Si l'extension n'est pas dans le tableau
                                if (!in_array($extension, $extensions)) {
                                    $i++;
                                    $erreur = 'Vous devez uploader un fichier de type png, gif, jpg, jpeg';
                                } //Si la taille du fichier dépasse la taille maximale autorisée
                                elseif ($tailleimg > $taille_maxi || $tailleimg == FALSE) {
                                    $i++;
                                    $erreur = 'Le fichier est trop gros... il doit faire moins de 2mo!';
                                }

                                //Vérification de la variable qui ne doit pas contenir d'erreur
                                if (empty($erreur)) {

                                    //On récupère le chemin d'accès jusqu'au répertoire de destination
                                    $repertoireDestination = getcwd() . "/content/data/vehicles/" . $title . "/";

                                    //Vérifie si le répértoire existe sinon le créer avec les droits pour l'éditez les fichiers à l'intérieur
                                    if (!file_exists($repertoireDestination)) {
                                        mkdir($repertoireDestination, 0777);
                                    }

                                    //On définit un nom de fichier
                                    $nomFichier = $title . $extension;

                                    //On formate le nom du fichier ici
                                    $fichier = strtr($nomFichier,
                                        'ÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÒÓÔÕÖÙÚÛÜÝàáâãäåçèéêëìíîïðòóôõöùúûüýÿ',
                                        'AAAAAACEEEEIIIIOOOOOUUUUYaaaaaaceeeeiiiioooooouuuuyy');

                                    //On remplace l'ancien nom du fichier par le nouveau
                                    $fichier = preg_replace('/([^.a-z0-9]+)/i', '-', $fichier);

                                    $picture = $repertoireDestination . $fichier;
                                    chmod($tmp_file, 0755);

                                    //Si la fonction renvoie false, c'est que ça n'a pas fonctionné...
                                    if (!rename($tmp_file, $picture)) {
                                        $i++;
                                        $erreur = "Echec de l'upload !";
                                    }
                                } else {
                                    throw new Exception($erreur);
                                }
                                $list = array(array("content/data/vehicles/" . $title . "/" . $fichier)
                                );

                                //Créer le fichier CSV et l'ouvre pour écrire dedans
                                $fp = fopen('content/data/vehicles/' . $title . '/' . $title . '.csv', 'a+');

                                //Met le chemin de l'image dans le fichier CSV
                                foreach ($list as $fields) {
                                    fputcsv($fp, $fields);
                                }

                                fclose($fp);

                                $repertoireIMG = "/content/data/vehicles/" . $title . "/" . $fichier;
                                $query = $bdd->prepare('UPDATE vehicles SET brand="' . $brand . '", model="' . $model . '", registrationNumber=' . $registrationNumber . ', locationPricePerDay=' . $locationPrice . ', mileage=' . $mileage . ', gearBox="' . $gearBox . '", doors=' . $doors . ', picture="' . $repertoireIMG . '" WHERE idVehicle="' . $vID . '"');

                                $query->execute();
                            }
                        }
                        else
                        {
                            $erreur = "Le champs 'Boîte de vitesse' ne contient pas manuel ou automatique !";
                            throw new Exception($erreur);
                        }
                    }
                    else
                    {
                        $erreur = "Le format du matricule est incorrect !";
                        throw new Exception($erreur);
                    }
                }
                else
                {
                    $erreur = "Le n° de matricule est invalide !";
                    throw new Exception($erreur);
                }
            }
            else
            {
                $erreur = "Le prix doit être un nombre !";
                throw new Exception($erreur);
            }
        }
        else
        {
            $erreur = "Le nombre de portes est incohérent !";
            throw new Exception($erreur);
        }
    }
    else
    {
        $erreur = "Certains champs sont vides !";
        throw new Exception($erreur);
    }
}

/***
 * @param $vID
 * This function is used to delete a vehicle
 */
function vehicleDelete($vID)
{
    //Connexion à la base de données
    $bdd = getDB();

    //Préparation et execution de la requête qui va supprimer le véhicule séléctionné
    $query = $bdd->prepare('DELETE FROM vehicles WHERE idVehicle='.$vID);
    $query->execute();
}

/***
 * @param $data
 * This function is used to add the informations of the user's booking in the database
 */
function addReservation($data)
{
    //Connexion à la base de données
    $bdd = getDB();

    //Préparation et execution de la requête qui va insérer les données de la réservation dans la base de données
    $query = $bdd->prepare('INSERT INTO booking (bookingDateStart, bookingHourWithdrawal, bookingDateEnd, bookingHourDelivery, bookingPaiementState, fkusers, fkvehicles) VALUES(:bookingDateStart, :bookingHourWithdrawal, :bookingDateEnd, :bookingHourDelivery, :bookingPaiementState, :fkusers, :fkvehicles)');

    $query->execute(array(
        'bookingDateStart' => $data['dateStart'],
        'bookingHourWithdrawal' => $data['hourStart'],
        'bookingDateEnd' => $data['dateEnd'],
        'bookingHourDelivery' => $data['hourEnd'],
        'bookingPaiementState' => "0",
        'fkusers' => $data['user'],
        'fkvehicles' => $data['vehicle']
    ));
}

/***
 * @param $uID
 * @return array
 * This function is used get the informations from the database to list all the booking of an user
 */
function listBookings($uID)
{
    //Connexion à la base de données
    $bdd = getDB();

    //Préparation et execution de la requête qui va récuperer toutes les informations concernant les réservations de l'utilisateur
    $query = $bdd->prepare('SELECT idBooking,bookingDateStart, bookingHourWithdrawal, bookingDateEnd, bookingHourDelivery, bookingPaiementState FROM booking WHERE fkusers='.$uID);
    $query->execute();

    //Retourne un tableau avec toutes les informations
    return $query->fetchAll();
}

/***
 * @param $data
 * @return array
 * @throws Exception : If the informations aren't consitent then throws an exception.
 * This function is used to check the informations of a booking
 */
function checkBooking($data)
{
    //Vérification si la date de départ est bien supérieure à aujourd'hui
    if (strtotime($data['startBooking']) > strtotime(date("j F, Y")))
    {
        //Vérification si la date de départ est inférieure à la date de retour
        if (strtotime($data['startBooking']) < strtotime($data['endBooking']))
        {
            //Recupération des POST et les transférer dans des variables depuis la méthode "Extract"
            extract($data);

            //Conversion du format de l'heure afin qu'il soit lisible par la base de données
            $dateStart=date("Y-m-d", strtotime($startBooking));
            $hourStart=$hourStartBooking;
            $dateEnd=date("Y-m-d", strtotime($endBooking));
            $hourEnd=$hourEndBooking;

            //Insertion de toutes les données dans un tableau
            $infoBooking=array(
                'dateStart' => $dateStart,
                'hourStart' => $hourStart,
                'dateEnd' => $dateEnd,
                'hourEnd' => $hourEnd
            );

            //Retourne le tableau
            return $infoBooking;
        }
        else
        {
            $erreur = '2';
            throw new Exception($erreur);
        }
    }
    else
    {
        $erreur = '1';
        throw new Exception($erreur);
    }
}

/***
 * @return array
 * This function is used to create an array from the data in $_SESSIONS
 */
function createArrayBook()
{
    //On récupère les informations dans les variables $_SESSION et on les insère dans un tableau
    $dataBooking = array(
        'dateStart' => $_SESSION['booking']['dateStart'],
        'hourStart' => $_SESSION['booking']['hourStart'],
        'dateEnd' => $_SESSION['booking']['dateEnd'],
        'hourEnd' => $_SESSION['booking']['hourEnd'],
        'user' => $_SESSION['uID'],
        'vehicle'=>$_GET['vID']
    );

    //On retourne le tableau
    return $dataBooking;
}

/***
 * @param $vID
 * @return array
 * This function is used to get the booking's history of a vehicle
 */
function getVehicleHistoryInfo($vID)
{
    //Connexion à la base de données
    $bdd = getDB();

    //Préparation et execution de la requête qui va récupérer les informations des réservations effectuées par un utilisateur pour un véhicule
    $query = $bdd->prepare('SELECT * FROM booking LEFT JOIN vehicles ON booking.fkvehicles=vehicles.idVehicle LEFT JOIN users ON booking.fkusers=users.idUser WHERE fkvehicles='.$vID);
    $query->execute();

    //On place les données dans un tableau et on le retourne
    return $query->fetchAll();;
}

/***
 * @param $uID
 * @return array
 * This function is used to get the booking's history of a client
 */
function getClientHistoryInfo($uID)
{
    //Connexion à la base de données
    $bdd = getDB();

    //Préparation et execution de la requête qui va récupérer les informations des réservations effectuées par un utilisateur pour un véhicule
    $query = $bdd->prepare('SELECT * FROM booking LEFT JOIN vehicles ON booking.fkvehicles=vehicles.idVehicle LEFT JOIN users ON booking.fkusers=users.idUser WHERE fkusers='.$uID);
    $query->execute();

    //On place les données dans un tableau
    $array=$query->fetchAll();

    //Vérification du contenu du tableau
    if(empty($array))
    {
        //Préparation et execution de la requête qui va récupérer les informations du client
        $query = $bdd->prepare('SELECT firstNameUser, lastNameUser FROM users WHERE idUser='.$uID);
        $query->execute();

        //On place les données dans un tableau
        $array=$query->fetchAll();
    }

    //On retourne le tableau
    return $array;
}

