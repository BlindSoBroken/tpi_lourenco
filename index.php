<?php
/**
 * Created by PhpStorm.
 * User: Bruno.DA-COSTA-LOURE
 * Date: 14.05.2019
 * Time: 09:15
 */

    session_start();

    //Require plante si il ne trouve pas le fichier demandé (Include continue sans)
    require "controller/controller.php";

    //Gestion d'exception
    try
    {
        //Test le contenu de la variable $action
        if (isset($_GET['action']))
        {
            $action = $_GET['action'];
            //En fonction du contenu de la variable $action, le code va appeler une fonction
            switch ($action)
            {
                case 'view_home' :
                    home("","");
                    break;

                case 'view_register' :
                    register("");
                    break;

                case 'view_register_complete' :
                    registerData($_POST);
                    break;

                case 'view_logout' :
                    logout();
                    break;

                case 'view_login' :
                    login("");
                    break;

                case 'view_login_data' :
                    login_data($_POST);
                    break;

                case 'view_profile' :
                    profile($data=$_SESSION['login'],"");
                    break;

                case 'view_profileData':
                    profileData($data=$_SESSION['login']);
                    break;

                case 'view_clientsManagement' :
                    clientsManagement();
                    break;

                case 'view_vehiclesManagement' :
                    vehiclesManagement("");
                    break;

                case 'view_vehiclesAdd' :
                    vehicleAddingForm("");
                    break;

                case 'view_vehiclesAddData' :
                    vehicleAddData($_POST);
                    break;

                case 'view_vehicle' :
                    vehicle_consult();
                    break;

                case 'view_vehicleModify' :
                    vehicle_modify("");
                    break;

                case 'view_vehiclesModifyData' :
                    vehicleModifyData($_POST);
                    break;

                case 'view_vehicleDelete' :
                    vehicle_delete("");
                    break;

                case 'view_listVehicles' :
                    listVehicles($_POST,"");
                    break;

                case 'view_addbooking' :
                    addBooking();
                    break;

                case 'view_mybookings' :
                    myBookings("");
                    break;

                case 'view_clientDelete' :
                    client_delete();
                    break;

                case 'view_clientModify' :
                    client_ModifyForm("");
                    break;

                case 'view_vehicleHistory':
                    vehicleHistory();
                    break;

                case 'view_clientModifyData':
                    clientModifyData($_POST);
                    break;

                case 'view_clientHistory':
                    clientHistory();
                    break;
                default :
                    throw new Exception("Action non valide");
            }
        }
        else
        {
            //Si la variable ne contient rien alors le code appelle la fonction accueil()
            home("","");
        }
    }
    catch (Exception $e)
    {
        erreur($e->getMessage());
    }


