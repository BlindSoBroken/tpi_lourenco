<?php
/**
 * Created by PhpStorm.
 * User: Bruno.DA-COSTA-LOURE
 * Date: 14.05.2019
 * Time: 11:08
 */
ob_start();
$titre="Connexion réussie";
?>
<html>
    <body>
    <section class="site-hero overlay" style="background-image: url(../content/assets/images/hero_5.jpg)" data-stellar-background-ratio="0.5" id="section-home">
        <div class="container">
            <div class="row site-hero-inner justify-content-center align-items-center">
                <div class="col-md-12" data-aos="fade-up">
                    <div style="padding-top: 2%; padding-bottom: 4%;padding-right: 2%; width: 65%; margin: 0 auto;" class="box_title">
                        <div class="container" style="padding-top: 3%">
                            <div class="row">
                                <?php
                                //Affiche un message de bienvenue avec le nom de l'utilisateur
                                echo '<div class="col-md-12 justify-content-center align-items-center">
                                        <h1>Bienvenue <b>'.$_SESSION['login'].'</b></h1>
                                      </div>';
                                ?>
                                <div class="col-md-12 justify-content-center align-items-center">
                                    <h3> Vous êtes désormais connecté(e) sur AutoSwissRent</h3>
                                </div>
                                <div class="col-md-12"></div>
                                <?php
                                    //Vérification du type d'utilisateur
                                    if ($_SESSION['categoryUser']==0)
                                    {
                                ?>
                                        <div class="col-md-6 col-lg-4 align-self-end">
                                            <button class="btn btn-primary btn-block text-white"
                                                    onclick="document.location.href='index.php?action=view_home'">Retour
                                                à l'accueil
                                            </button>
                                        </div>
                                        <div class="col-md-6 col-lg-4 align-self-end">
                                            <button class="btn btn-primary btn-block text-white"
                                                    onclick="document.location.href='index.php?action=view_vehiclesManagement'">
                                                Gestion véhicules
                                            </button>
                                        </div>
                                        <div class="col-md-6 col-lg-4 align-self-end">
                                            <button class="btn btn-primary btn-block text-white"
                                                    onclick="document.location.href='index.php?action=view_clientsManagement'">
                                                Gestion clients
                                            </button>
                                        </div>

                                <?php
                                    }
                                    else
                                    {
                                ?>
                                        <div class="col-md-6 col-lg-4 align-self-end">
                                            <button class="btn btn-primary btn-block text-white"
                                                    onclick="document.location.href='index.php?action=view_home'">Retour
                                                à l'accueil
                                            </button>
                                        </div>
                                        <div class="col-md-6 col-lg-4 align-self-end">
                                            <button class="btn btn-primary btn-block text-white"
                                                    onclick="document.location.href='index.php?action=view_profile'">
                                                Mon profil
                                            </button>
                                        </div>
                                        <div class="col-md-6 col-lg-4 align-self-end">
                                            <button class="btn btn-primary btn-block text-white"
                                                    onclick="document.location.href='index.php?action=view_mybookings'">
                                                Mes réservations
                                            </button>
                                        </div>
                                <?php
                                    }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- END section -->
    </body>
</html>
<?php
$contenu = ob_get_clean();
require "gabarit.php";
