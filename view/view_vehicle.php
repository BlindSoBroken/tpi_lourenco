<?php
/**
 * Created by PhpStorm.
 * User: Bruno Lourenço
 * Date: 19.05.2019
 * Time: 14:15
 */
ob_start();
$titre=$vehicle['brand'] . " " . $vehicle['model'];
?>
<section class="site-hero overlay" style="background-image: url(../content/assets/images/hero_5.jpg)" data-stellar-background-ratio="0.5" id="section-home">
    <div class="container">
        <div class="row site-hero-inner justify-content-center align-items-center">
            <div class="col-md-12" data-aos="fade-up">
                <div style="padding-top: 2%; padding-bottom: 4%;padding-right: 2%; width: 100%; margin-top: 15%; margin-bottom: 5%;" class="box_title">
                    <div class="container">
                        <H1 style="padding-bottom: 20px"><b><?= $vehicle['brand'] . " " . $vehicle['model'] ?></b></H1>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="col-md-12"><H3><u>Détails de la voiture</u></H3></div>
                                <div class="col-md-12">Numéro de matricule : <?= $vehicle['registrationNumber']?></div>
                                <div class="col-md-12">Kilomètres au compteur : <?= $vehicle['mileage']?> Km</div>
                                <div class="col-md-12">Boîte de vitesse : <?= $vehicle['gearBox']?></div>
                                <div class="col-md-12">Nombre de portes : <?= $vehicle['doors']?></div>
                                <?php if($_SESSION['categoryUser']==1){echo'?><div class="col-md-12">Prix par jour :'.$vehicle['locationPricePerDay'].' .- CHF</div><?php';}?>
                                <div class="col-md-12" style="padding-top: 5%;"><H4><b><u>Total :</u> <?php if($_SESSION['categoryUser']==1){echo $vehicle['locationPricePerDay'] * $_SESSION['nbDays'];}else {echo $vehicle['locationPricePerDay'];} ?>.- CHF</b></H4></div>
                                <?php if (isset($_SESSION['login']))
                                {

                                ?>
                                    <div class="col-md-4" style="padding-top: 12%"><a href="index.php?action=view_addbooking&vID=<?=$vehicle['idVehicle']?>"><button class="btn btn-primary btn-block text-white" type="submit" name="POST">Louer</button></a></div>
                                <?php
                                }
                                ?>
                            </div>
                            <div class="col-md-6"><img height="360px" width="480px" src="<?= $vehicle['picture']?>"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
$contenu = ob_get_clean();
require "gabarit.php";
