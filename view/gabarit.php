<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>AutoSwissRent - <?=$titre?> </title>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=|Roboto+Sans:400,700|Playfair+Display:400,700">

        <link rel="stylesheet" href="/content/assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="/content/assets/css/animate.css">
        <link rel="stylesheet" href="/content/assets/css/owl.carousel.min.css">
        <link rel="stylesheet" href="/content/assets/css/aos.css">
        <link rel="stylesheet" href="/content/assets/css/bootstrap-datepicker.css">
        <link rel="stylesheet" href="/content/assets/css/jquery.timepicker.css">
        <link rel="stylesheet" href="/content/assets/css/fancybox.min.css">

        <link rel="stylesheet" href="/content/assets/fonts/ionicons/css/ionicons.min.css">
        <link rel="stylesheet" href="/content/assets/fonts/fontawesome/css/font-awesome.min.css">

        <!-- Theme Style -->
        <link rel="stylesheet" href="/content/assets/css/style.css">
    </head>
    <body data-spy="scroll" data-target="#templateux-navbar" data-offset="200">
        <header>
            <?php
                //Sert à affiché le menu du haut
                require "content/src/includes/menu.php";
            ?>
        </header>
        <!-- Sert à afficher le contenu des vues -->
        <div class="contenu">
            <?=$contenu?>
        </div>

        <footer>
            <?php
                //Affiche le pied de page
                require "content/src/includes/footer.php";
            ?>
        </footer>

        <script src="/content/assets/js/jquery-3.3.1.min.js"></script>
        <script src="/content/assets/js/jquery-migrate-3.0.1.min.js"></script>
        <script src="/content/assets/js/popper.min.js"></script>
        <script src="/content/assets/js/bootstrap.min.js"></script>
        <script src="/content/assets/js/owl.carousel.min.js"></script>
        <script src="/content/assets/js/jquery.stellar.min.js"></script>
        <script src="/content/assets/js/jquery.fancybox.min.js"></script>
        <script src="/content/assets/js/jquery.easing.1.3.js"></script>
        <script src="/content/assets/js/aos.js"></script>
        <script src="/content/assets/js/bootstrap-datepicker.js"></script>
        <script src="/content/assets/js/jquery.timepicker.min.js"></script>
        <script src="/content/assets/js/main.js"></script>
    </body>
</html>