<?php
/**
 * Created by PhpStorm.
 * User: Bruno.DA-COSTA-LOURE
 * Date: 16.05.2019
 * Time: 14:13
 */
ob_start();
$titre="Gestions Clients";
?>
<section class="site-hero overlay" style="background-image: url(../content/assets/images/hero_5.jpg)" data-stellar-background-ratio="0.5" id="section-home">
    <div class="container">
        <div class="row site-hero-inner justify-content-center align-items-center">
            <div class="col-md-12" data-aos="fade-up">
                <div style="padding-top: 2%; padding-bottom: 4%;padding-right: 38px;padding-left: 40px; width: 100%; margin: 0 auto;" class="box_title">
                    <div class="container">
                        <H1 style="padding-bottom: 20px">Listes des véhicules</H1>
                        <?php
                            //Vérification si le modèle nous renvoie un message d'erreur
                            if($erreur=="true")
                            {
                                echo "<font STYLE='text-decoration:underline' color='red'>Le véhicule est en cours de location par un client ! <br/><br/></font>";
                            }
                        ?>
                        <div class="row" style="padding-bottom: 2%;color: #FFFFFF; background-color: black;">
                            <div class="col-md-2 gestionAdmin">
                                <b>Marque | Modèle</b>
                            </div>
                            <div class="col-md-1 gestionAdmin">
                                <b>Matricule</b>
                            </div>
                            <div class="col-md-1 gestionAdmin">
                                <b>Kilomètres</b>
                            </div>
                            <div class="col-md-1 gestionAdmin">
                                <b>Boîte de vitesse</b>
                            </div>
                            <div class="col-md-1 gestionAdmin">
                                <b>Portes</b>
                            </div>
                            <div class="col-md-1 gestionAdmin">
                                <b>Prix</b>
                            </div>
                            <div class="col-md-1 gestionAdmin">
                                <b>Consulter</b>
                            </div>
                            <div class="col-md-1 gestionAdmin">
                                <b>Historique</b>
                            </div>
                            <div class="col-md-1 gestionAdmin">
                                <b>Modifier</b>
                            </div>
                            <div class="col-md-1 gestionAdmin">
                                <b>Dispo</b>
                            </div>
                            <div class="col-md-1 gestionAdmin">
                                <b>Suppr</b>
                            </div>
                        </div>
                        <div class="row">
                            <?php foreach ($gestion as $vehicles):?>
                                <div class="col-md-2 gestionAdmin" style="border-left: solid black 2px;">
                                    <?= $vehicles['brand']." ".$vehicles['model']?>
                                </div>
                                <div class="col-md-1 gestionAdmin">
                                    <?= $vehicles['registrationNumber']?>
                                </div>
                                <div class="col-md-1 gestionAdmin">
                                    <?= $vehicles['mileage']?>
                                </div>
                                <div class="col-md-1 gestionAdmin">
                                    <?= $vehicles['gearBox']?>
                                </div>
                                <div class="col-md-1 gestionAdmin">
                                    <?= $vehicles['doors']?>
                                </div>
                                <div class="col-md-1 gestionAdmin">
                                    <?= $vehicles['locationPricePerDay']?>
                                </div>
                                <div class="col-md-1 gestionAdmin">
                                    <a href='index.php?action=view_vehicle&vID=<?=$vehicles['idVehicle']?>'><img class="gestionIMG" src="../content/assets/images/consulter.png"></a>
                                </div>
                                <div class="col-md-1 gestionAdmin">
                                    <a href='index.php?action=view_vehicleHistory&vID=<?=$vehicles['idVehicle']?>'><img class="gestionIMG" src="../content/assets/images/history.png"></a>
                                </div>
                                <div class="col-md-1 gestionAdmin">
                                    <a href='index.php?action=view_vehicleModify&vID=<?=$vehicles['idVehicle']?>'><img class="gestionIMG" src="../content/assets/images/edit.png"></a>
                                </div>
                                <div class="col-md-1 gestionAdmin">
                                    <?php if ($vehicles['disponibility']==1)
                                        {
                                            $vehicles['disponibility']="Oui";
                                        }
                                        elseif ($vehicles['disponibility']==0)
                                        {
                                            $vehicles['disponibility']="Non";
                                        }
                                        echo $vehicles['disponibility'];
                                    ?>
                                </div>
                                <div class="col-md-1 gestionAdmin" style="border-right: solid black 2px">
                                    <a href='index.php?action=view_vehicleDelete&vID=<?=$vehicles['idVehicle']?>'><img class="gestionIMG" src="../content/assets/images/delete.png"></a>
                                </div>
                            <?php endforeach; ?>
                            <div class="col-md-12" style="padding-top: 3%;" >
                                <a href="index.php?action=view_vehiclesAdd"><img style="margin-left: auto; margin-right: auto; display: block;" src="../content/assets/images/add.png"></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
$contenu = ob_get_clean();
require "gabarit.php";
