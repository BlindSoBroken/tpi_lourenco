<?php
/**
 * Created by PhpStorm.
 * User: Bruno Lourenço
 * Date: 19.05.2019
 * Time: 17:16
 */
ob_start();
$titre="Véhicule - Modification";
?>
    <section class="site-hero overlay" style="background-image: url(../content/assets/images/hero_5.jpg)" data-stellar-background-ratio="0.5" id="section-home">
        <div class="container">
            <div class="row site-hero-inner justify-content-center align-items-center">
                <div class="col-md-12" data-aos="fade-up">
                    <div style="padding-top: 2%; padding-bottom: 4%;padding-right: 2%; width: 100%; margin-top: 10%;" class="box_title">
                        <div class="container">
                            <H1>Modification d'un véhicule</H1>
                            <form method="POST" action="index.php?action=view_vehiclesModifyData&vID=<?=$vehicleInfo['idVehicle']?>" enctype="multipart/form-data">
                                <?php
                                //Vérification si le modèle nous renvoie un message d'erreur
                                if($erreur)
                                {
                                    echo "<font STYLE='text-decoration:underline' color='red'>".$erreur."<br/><br/></font>";
                                }
                                else
                                {
                                    echo"";
                                }
                                ?>
                                <div class="register-top-grid">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <span>Marque<label>*</label></span>
                                            <input type="text" name="brandVehicle" value="<?=$vehicleInfo['brand']?>">
                                        </div>
                                        <div class="col-md-6">
                                            <span>Modèle<label>*</label></span>
                                            <input type="text" name="modelVehicle" value="<?=$vehicleInfo['model']?>">
                                        </div>
                                        <div class="col-md-6">
                                            <span>Matricule<label>*</label></span>
                                            <input type="text" name="registrationNumberVehicle" value="<?=$vehicleInfo['registrationNumber']?>">
                                        </div>
                                        <div class="col-md-6">
                                            <span>Prix de location par jour<label>*</label></span>
                                            <input type="text" maxlength="4" name="locationPriceVehicle" value="<?=$vehicleInfo['locationPricePerDay']?>">
                                        </div>
                                        <div class="col-md-6">
                                            <span>kilométrage<label>*</label></span>
                                            <input type="text" name="mileageVehicle" value="<?=$vehicleInfo['mileage']?>">
                                        </div>
                                        <div class="col-md-6">
                                            <span>Boîte de vitesse<label>*</label></span>
                                            <input type="text" name="gearBoxVehicle" value="<?=$vehicleInfo['gearBox']?>">
                                        </div>
                                        <div class="col-md-6">
                                            <span>Nombre de portes<label>*</label></span>
                                            <input type="text" name="doorsVehicle" value="<?=$vehicleInfo['doors']?>">
                                        </div>
                                        <div class="col-md-6">
                                            <input type="file" name="imgVehicle" style="padding-top:40px" />
                                            <input type="hidden" name="MAX_FILE_SIZE" value="2097152" />
                                        </div>
                                        <div class="col-lg-5"></div>
                                        <div class="col-md-6 col-lg-2 align-self-end" style="padding-right: 40px; margin-top: 40px">
                                            <button class="btn btn-primary btn-block text-white" style="margin-left: auto; margin-right: auto; display block" type="submit" name="POST">Modifier</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php
$contenu = ob_get_clean();
require "gabarit.php";