<?php
/**
 * Created by PhpStorm.
 * User: Bruno.DA-COSTA-LOURE
 * Date: 16.05.2019
 * Time: 14:13
 */
ob_start();
$titre="Gestions Clients";
?>

<section class="site-hero overlay" style="background-image: url(../content/assets/images/hero_5.jpg)" data-stellar-background-ratio="0.5" id="section-home">
    <div class="container">
        <div class="row site-hero-inner justify-content-center align-items-center">
            <div class="col-md-12" data-aos="fade-up">
                <div style="padding-top: 2%; padding-bottom: 4%;padding-right: 38px;padding-left: 40px; width: 100%; margin: 0 auto;" class="box_title">
                    <div class="container">
                        <H1 style="padding-bottom: 20px">Listes des clients</H1>
                        <div class="row" style="padding-bottom: 2%;color: #FFFFFF; background-color: black;">
                            <div class="col-md-1 gestionAdmin">
                                <b>Nom</b>
                            </div>
                            <div class="col-md-1 gestionAdmin">
                                <b>Prénom</b>
                            </div>
                            <div class="col-md-3 gestionAdmin">
                                <b>Adresse</b>
                            </div>
                            <div class="col-md-2 gestionAdmin">
                                <b>Nombre locations</b>
                            </div>
                            <div class="col-md-2 gestionAdmin">
                                <b>Paiements</b>
                            </div>
                            <div class="col-md-1 gestionAdmin">
                                <b>Historique</b>
                            </div>
                            <div class="col-md-1 gestionAdmin">
                                <b>Modifier</b>
                            </div>
                            <div class="col-md-1 gestionAdmin">
                                <b>Suppr</b>
                            </div>
                        </div>
                        <div class="row">
                            <?php foreach ($gestion as $clients):?>
                                <div class="col-md-1 gestionAdmin" style="border-left: solid black 2px;"><?= $clients['lastNameUser']?></div>
                                    <div class="col-md-1 gestionAdmin"><?=$clients['firstNameUser']?></div>
                                    <div class="col-md-3 gestionAdmin"><?= $clients['addressUser']?></div>
                                    <div class="col-md-2 gestionAdmin"><?php if(isset($clients['totalLocation'])){echo $clients['totalLocation'];}else{echo '0';}?></div>
                                    <div class="col-md-2 gestionAdmin">Paiements : à jour</div>
                                    <div class="col-md-1 gestionAdmin">
                                        <a href="index.php?action=view_clientHistory&uID=<?=$clients['idUser'] ?>"><img class="gestionIMG" src="../content/assets/images/history.png"></a>
                                    </div>
                                    <div class="col-md-1 gestionAdmin">
                                        <a href="index.php?action=view_clientModify&uID=<?=$clients['idUser'] ?>"><img class="gestionIMG" src="../content/assets/images/edit.png"></a>
                                    </div>
                                    <div class="col-md-1 gestionAdmin" style="border-right: solid black 2px">
                                        <a href="index.php?action=view_clientDelete&uID=<?=$clients['idUser'] ?>"><img class="gestionIMG" src="../content/assets/images/delete.png"></a>
                                    </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php
$contenu = ob_get_clean();
require "gabarit.php";