<?php
/**
 * Created by PhpStorm.
 * User: Bruno.DA-COSTA-LOURE
 * Date: 24.05.2019
 * Time: 09:24
 */
$titre="Mes réservations";
ob_start();
setlocale(LC_TIME, 'fr_FR.utf8','fra');
?>
    <section class="site-hero overlay" style="background-image: url(../content/assets/images/hero_5.jpg)" data-stellar-background-ratio="0.5" id="section-home">
        <div class="container">
            <div class="row site-hero-inner justify-content-center align-items-center">
                <div class="col-md-12" data-aos="fade-up">
                    <div style="padding-top: 2%; padding-bottom: 4%;padding-right: 38px; padding-left: 40px; width: 100%; margin:0 auto; margin-top: 15%; margin-bottom: 5%" class="box_title">
                        <div class="container">
                            <H1 style="padding-bottom: 20px">Mes réservations</H1>
                            <div class="row" style="padding-bottom: 2%;color: #FFFFFF; background-color: red;">
                                <div class="col-md-2 gestionBookings">
                                    <b>Date de retrait</b>
                                </div>
                                <div class="col-md-2 gestionBookings">
                                    <b>Heure de retrait</b>
                                </div>
                                <div class="col-md-2 gestionBookings">
                                    <b>Date de dépôt</b>
                                </div>
                                <div class="col-md-2 gestionBookings">
                                    <b>Heure de dépôt</b>
                                </div>
                                <div class="col-md-2 gestionBookings">
                                    <b>Êtat du paiement</b>
                                </div>
                                <div class="col-md-2 gestionBookings">
                                    Modifier
                                </div>
                            </div>
                            <div class="row">
                                <?php
                                //Vérification si l'utilisateur a déjà effecuté une réservation
                                if (empty($gestionBook))
                                {?>
                                    <div class="col-md-12">
                                        <h3 style="text-align: center;padding-top: 20px">Aucune réservation à venir</h3>
                                    </div>
                                <?php
                                }
                                else
                                {
                                    foreach ($gestionBook as $booking):?>
                                        <div class="col-md-2 gestionBookings" style="border-left: solid red 2px;">
                                            <?= strftime("%d %B %Y",strtotime($booking['bookingDateStart']));?>
                                        </div>
                                        <div class="col-md-2 gestionBookings">
                                            <?=strftime("%H:%M",strtotime($booking['bookingHourWithdrawal']))?>
                                        </div>
                                        <div class="col-md-2 gestionBookings">
                                            <?=strftime("%d %B %Y",strtotime($booking['bookingDateEnd']));?>
                                        </div>
                                        <div class="col-md-2 gestionBookings">
                                            <?=strftime("%H:%M",strtotime($booking['bookingHourDelivery']))?>
                                        </div>
                                        <div class="col-md-2 gestionBookings">
                                            <?php if($booking['bookingPaiementState']==1){echo "Payé";}else{echo "Non payé";}?>
                                        </div>
                                        <div class="col-md-2 gestionBookings" style="border-right: solid red 2px">
                                            <a href='index.php?action=view_vehicleModify&vID=<?=$booking['idBooking']?>'><img class="gestionIMG" src="../content/assets/images/edit.png"></a>
                                        </div>
                                    <?php endforeach;
                                }?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php
$contenu = ob_get_clean();
require "gabarit.php";
