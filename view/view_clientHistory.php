<?php
/**
 * Created by PhpStorm.
 * User: Bruno.DA-COSTA-LOURE
 * Date: 28.05.2019
 * Time: 16:14
 */
ob_start();
$titre="Historique";

setlocale(LC_TIME, 'fr_FR.utf8','fra');
?>
<section class="site-hero overlay" style="background-image: url(../content/assets/images/hero_5.jpg)" data-stellar-background-ratio="0.5" id="section-home">
    <div class="container">
        <div class="row site-hero-inner justify-content-center align-items-center">
            <div class="col-md-12" data-aos="fade-up">
                <div style="padding-top: 2%; padding-bottom: 4%;padding-right: 38px;padding-left: 40px; width: 100%; margin:0 auto; margin-top: 15%; margin-bottom: 5%" class="box_title">
                    <div class="container">
                        <H1 style="padding-bottom: 20px">Historique de : <b><?=$history[0]['firstNameUser']. " " .$history[0]['lastNameUser']?></b></H1>
                        <div class="row" style="padding-bottom: 2%;color: #FFFFFF; background-color: black;border-right: solid white 2px">
                            <div class="col-md-1 gestionAdmin">
                                <b>ID du véhicule</b>
                            </div>
                            <div class="col-md-2 gestionAdmin">
                                <b>Marque | Modèle</b>
                            </div>
                            <div class="col-md-2 gestionAdmin">
                                <b>Date de début</b>
                            </div>
                            <div class="col-md-2 gestionAdmin">
                                <b>Date de fin</b>
                            </div>
                            <div class="col-md-1 gestionAdmin">
                                <b>Durée en jour(s)</b>
                            </div>
                            <div class="col-md-1 gestionAdmin">
                                <b>Prix par jour</b>
                            </div>
                            <div class="col-md-1 gestionAdmin">
                                <b>Prix total</b>
                            </div>
                            <div class="col-md-2 gestionAdmin">
                                <b>État du paiement</b>
                            </div>
                        </div>
                        <div class="row">
                            <?php
                            //Vérification si l'utilisateur a déjà effectué une réservation
                            if (!isset($history['1']['firstNameUser']))
                            {?>
                            <div class="col-md-12">
                                <h3 style="text-align: center;padding-top: 20px">Aucune réservation n'a été effectuée par l'utilisateur</h3>
                            </div>
                            <?php
                                }
                            else
                            {
                             foreach ($history as $info):?>
                                <div class="col-md-1 gestionAdmin" style="border-left: solid black 2px;">
                                    <?= $info['idVehicle']?>
                                </div>
                                <div class="col-md-2 gestionAdmin" style="border-left: solid black 2px;">
                                    <?= $info['brand']. " " .$info['model']?>
                                </div>
                                <div class="col-md-2 gestionAdmin">
                                    <?= strftime("%d %B %Y",strtotime($info['bookingDateStart']));?>
                                </div>
                                <div class="col-md-2 gestionAdmin">
                                    <?= strftime("%d %B %Y",strtotime($info['bookingDateEnd']));?>
                                </div>
                                <div class="col-md-1 gestionAdmin">
                                    <?php
                                            $diff=strtotime($info['bookingDateEnd']) - strtotime($info['bookingDateStart']);
                                            echo round($diff / (60*60*24));
                                    ?>
                                </div>
                                <div class="col-md-1 gestionAdmin">
                                    <?=$info['locationPricePerDay']?>.-
                                </div>
                                <div class="col-md-1 gestionAdmin">
                                    <?php
                                    $diff=strtotime($info['bookingDateEnd']) - strtotime($info['bookingDateStart']);
                                    echo $info['locationPricePerDay'] * round($diff / (60*60*24));
                                    ?>.-
                                </div>
                                <div class="col-md-2 gestionAdmin" style="border-right: solid black 2px;">
                                    <?php if($info['bookingPaiementState']==1){echo "Payé";}else{echo "Non payé";}?>
                                </div>
                                <?php endforeach;
                            }?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
$contenu = ob_get_clean();
require "gabarit.php";
