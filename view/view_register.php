<?php
/**
 * Created by PhpStorm.
 * User: Bruno.DA-COSTA-LOURE
 * Date: 14.05.2019
 * Time: 11:08
 */
ob_start();
$titre="Inscription";
?>
<section class="site-hero overlay" style="background-image: url(../content/assets/images/hero_5.jpg)" data-stellar-background-ratio="0.5" id="section-home">
    <div class="container">
        <div class="row site-hero-inner justify-content-center align-items-center">
            <div class="col-md-12" data-aos="fade-up">
                <div style="padding-top: 2%; padding-bottom: 4%;padding-right: 2%; width: 100%; margin-top: 10%;" class="box_title">
                    <div class="container">
                        <H1>Inscription</H1>
                        <form method="POST" action="index.php?action=view_register_complete" enctype="multipart/form-data">
                            <?php
                            //Vérification si le modèle nous renvoie un message d'erreur
                            switch($error)
                            {
                                case 1:
                                    echo "<font STYLE='text-decoration:underline' color='red'>Certains champs sont vides ! <br/><br/></font>";
                                    break;

                                case 2:
                                    echo "<font STYLE='text-decoration:underline' color='red'>Le code postal est incorrect ! <br/> <br/></font>";
                                    break;

                                case 3:
                                    echo "<font STYLE='text-decoration:underline' color='red'>Le format de l'adresse mail est invalide ! <br/> <br/></font>";
                                    break;

                                case 4:
                                    echo "<font STYLE='text-decoration:underline' color='red'>Le mot de passe ne correspond pas aux critères de sécurité demandé ! <br/> <br/></font>";
                                    breaK;

                                case 5:
                                    echo "<font STYLE='text-decoration:underline' color='red'>Les mots de passes ne sont pas identiques ! <br/> <br/></font>";
                                    break;

                                case 6:
                                    echo "<font STYLE='text-decoration:underline' color='red'>Les informations personnelle ne sont pas valide ! <br/> <br/></font>";
                                    break;

                                case 7:
                                    echo "<font STYLE='text-decoration:underline' color='red'>Les informations de connexion ne sont pas valide ! <br/> <br/></font>";
                                    break;

                                default:
                                    echo "";
                            }
                            ?>
                            <div class="register-top-grid">
                                <h3>INFORMATIONS PERSONNELLE</h3>
                                <div class="row">
                                    <div class="col-md-6">
                                        <span>Nom<label>*</label></span>
                                        <input type="text" name="inscriptionNom" value="<?=@$_POST['inscriptionNom']?>">
                                    </div>
                                    <div class="col-md-6">
                                        <span>Prénom<label>*</label></span>
                                        <input type="text" name="inscriptionPrenom" value="<?=@$_POST['inscriptionPrenom']?>">
                                    </div>
                                    <div class="col-md-6">
                                        <span>Adresse<label>*</label></span>
                                        <input type="text" name="inscriptionAdresse" value="<?=@$_POST['inscriptionAdresse']?>">
                                    </div>
                                    <div class="col-md-6">
                                        <span>NPA<label>*</label></span>
                                        <input type="text" maxlength="4" name="inscriptionNPA" value="<?=@$_POST['inscriptionNPA']?>">
                                    </div>
                                    <div class="col-md-6">
                                        <span>Ville<label>*</label></span>
                                        <input type="text" name="inscriptionVille" value="<?=@$_POST['inscriptionVille']?>">
                                    </div>
                                    <div class="col-md-6">
                                        <span>Adresse mail<label>*</label></span>
                                        <input type="text" name="inscriptionMail" value="<?=@$_POST['inscriptionMail']?>">
                                    </div>
                                </div>
                            </div>
                            <div class="clear"> </div>
                            <div class="register-bottom-grid">
                                <h3 style="padding-top: 30px">INFORMATIONS DE CONNEXION</h3>
                                <div class="row">
                                    <div class="col-md-6">
                                        <span>Nom d'utilisateur<label>*</label></span>
                                        <input type="text" name="inscriptionUsername" value="<?=@$_POST['inscriptionUsername']?>">
                                    </div>
                                    <div class="col-md-6">
                                            <div class="wrap">
                                                <span style="height: 15px;">Mot de passe <label>*</label>
                                                    <img class="icon-secu" src="../content/assets/images/icone-info-secu.png">
                                                    <div class="popup">
                                                        <ul><H6 style="font-size: 1em"><u>Le mot de passe doit contenir au minimum :</u></H6>
                                                            <li style="color: #000;">Une majuscule</li>
                                                            <li style="color: #000;">Une minuscule</li>
                                                            <li style="color: #000;">Un chiffre</li>
                                                        </ul>
                                                    </div>
                                                </span>
                                            </div>
                                        <input type="password" name="inscriptionPassword">
                                    </div>
                                    <div class="col-md-6">
                                        <span>Confirmer le mot de passe<label>*</label></span>
                                        <input type="password" name="inscriptionPassword2">
                                    </div>
                                    <div class="col-lg-3"></div>
                                    <div class="col-md-6 col-lg-3 align-self-end">
                                        <button class="btn btn-primary btn-block text-white" type="submit" name="POST">M'inscrire</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
$contenu = ob_get_clean();
require "gabarit.php";
