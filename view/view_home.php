<?php
/**
 * Created by PhpStorm.
 * User: Bruno.DA-COSTA-LOURE
 * Date: 14.05.2019
 * Time: 09:15
 */

ob_start();
$titre="Accueil";

?>
<section class="site-hero overlay" style="background-image: url(../content/assets/images/hero_5.jpg)" data-stellar-background-ratio="0.5" id="section-home">
    <div class="container">
        <div class="row site-hero-inner justify-content-center align-items-center">
            <div class="col-md-10 text-center" data-aos="fade-up">
                <h1 class="heading">Nous croyons en nos jeunes conducteurs !</h1>
            </div>
        </div>
    </div>
</section>
    <!-- END section -->
<section class="section bg-dark pb-0"  >
    <div class="container">
        <div class="row check-availabilty" id="next">
            <div class="block-32" data-aos="fade-up" data-aos-offset="-200">
                <?php
                //Vérification de si un utilisateur s'est inscrit pour afficher un message de validation
                if ($registrationOK=="true")
                {
                    echo '<script>alert("Votre inscription a été validée avec succès. Vous pouvez désormais vous connecter !")</script>';
                }
                else
                {
                    echo "";
                }
                if (isset($erreur))
                {
                    //Vérification si le modèle nous renvoie un message d'erreur
                    switch($erreur)
                    {
                        case 1 :
                            echo "<font STYLE='text-decoration:underline' color='red'>La date de début doit être supérieur à aujourd'hui ! <br/><br/></font>";
                            break;

                        case 2 :
                            echo "<font STYLE='text-decoration:underline' color='red'>La date de fin doit être au minimum ultérieur d'un jour après la date de début ! <br/><br/></font>";
                            break;

                        default :
                            echo "";
                    }
                }
                ?>
                <form method="POST" action="index.php?action=view_listVehicles" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-md-6 mb-3 mb-lg-0 col-lg-3">
                            <label for="checkin_date" class="font-weight-bold text-black">Départ</label>
                            <div class="field-icon-wrap">
                                <div class="icon"><span class="icon-calendar"></span></div>
                                <input type="text" id="checkin_date" class="form-control" name="startBooking" value="<?php echo date("j F Y", strtotime('+1 day'));  ?>">
                            </div>
                        </div>
                        <div class="col-md-6 mb-3 mb-md-0 col-lg-2">
                            <div class="row">
                                <div class="col-md-12 mb-3 mb-md-0">
                                    <label for="adults" class="font-weight-bold text-black">Heure</label>
                                    <div class="field-icon-wrap">
                                        <div class="icon"><span class="ion-ios-arrow-down"></span></div>
                                        <select name="hourStartBooking" id="adults" class="form-control">
                                            <option value="01:00:00">01:00</option>
                                            <option value="02:00:00">02:00</option>
                                            <option value="03:00:00">03:00</option>
                                            <option value="04:00:00">04:00</option>
                                            <option value="05:00:00">05:00</option>
                                            <option value="06:00:00">06:00</option>
                                            <option value="07:00:00">07:00</option>
                                            <option value="08:00:00">08:00</option>
                                            <option value="09:00:00">09:00</option>
                                            <option selected value="10:00:00">10:00</option>
                                            <option value="11:00:00">11:00</option>
                                            <option value="12:00:00">12:00</option>
                                            <option value="13:00:00">13:00</option>
                                            <option value="14:00:00">14:00</option>
                                            <option value="15:00:00">15:00</option>
                                            <option value="16:00:00">16:00</option>
                                            <option value="17:00:00">17:00</option>
                                            <option value="18:00:00">18:00</option>
                                            <option value="19:00:00">19:00</option>
                                            <option value="20:00:00">20:00</option>
                                            <option value="21:00:00">21:00</option>
                                            <option value="22:00:00">22:00</option>
                                            <option value="23:00:00">23:00</option>
                                            <option value="24:00:00">24:00</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 mb-3 mb-lg-0 col-lg-3">
                            <label for="checkout_date" class="font-weight-bold text-black">Retour</label>
                            <div class="field-icon-wrap">
                                <div class="icon"><span class="icon-calendar"></span></div>
                                <input type="text" id="checkout_date" class="form-control" name="endBooking" value="<?php echo date("j F Y", strtotime('+2 day')); ?>">
                            </div>
                        </div>
                        <div class="col-md-6 mb-3 mb-md-0 col-lg-2">
                            <div class="row">
                                <div class="col-md-12 mb-3 mb-md-0">
                                    <label for="adults" class="font-weight-bold text-black">Heure</label>
                                    <div class="field-icon-wrap">
                                        <div class="icon"><span class="ion-ios-arrow-down"></span></div>
                                        <select name="hourEndBooking" id="adults" class="form-control">
                                            <option value="01:00:00">01:00</option>
                                            <option value="02:00:00">02:00</option>
                                            <option value="03:00:00">03:00</option>
                                            <option value="04:00:00">04:00</option>
                                            <option value="05:00:00">05:00</option>
                                            <option value="06:00:00">06:00</option>
                                            <option value="07:00:00">07:00</option>
                                            <option value="08:00:00">08:00</option>
                                            <option value="09:00:00">09:00</option>
                                            <option selected value="10:00:00">10:00</option>
                                            <option value="11:00:00">11:00</option>
                                            <option value="12:00:00">12:00</option>
                                            <option value="13:00:00">13:00</option>
                                            <option value="14:00:00">14:00</option>
                                            <option value="15:00:00">15:00</option>
                                            <option value="16:00:00">16:00</option>
                                            <option value="17:00:00">17:00</option>
                                            <option value="18:00:00">18:00</option>
                                            <option value="19:00:00">19:00</option>
                                            <option value="20:00:00">20:00</option>
                                            <option value="21:00:00">21:00</option>
                                            <option value="22:00:00">22:00</option>
                                            <option value="23:00:00">23:00</option>
                                            <option value="24:00:00">24:00</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-2 align-self-end">
                            <button class="btn btn-primary btn-block text-white" type="submit" name="POST">Chercher</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

<section class="py-5 bg-dark" id="section-about">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md-12 col-lg-7 ml-auto order-lg-2 position-relative mb-5" data-aos="fade-up">
                <img src="../content/assets/images/hero_4.jpg" alt="Image" class="img-fluid rounded">
            </div>
            <div class="col-md-12 col-lg-4 order-lg-1" data-aos="fade-up">
                <h2 class="heading mb-4" style="color: #FFFFFF">Actualités</h2>
                <p class="mb-5" style="color: #FFFFFF">Nous sommes enfin sur Berne et Zürich ! Deux nouvelles succursales ont été inaugurée le 15 Mai 2019 à l'aéroport de Berne et Zürich !</p>
                <p class="mb-5" style="color: #FFFFFF">We are finally on Bern and Zürich ! Two new branches were inaugurated on May 15, 2019 at Bern Airport and Zurich !</p>
                <p class="mb-5" style="color: #FFFFFF">Wir sind endlich in Bern und Zürich ! Zwei neue Filialen wurden am 15. Mai 2019 am Flughafen Bern und in Zürich eingeweiht !</p>
                <p><a href="https://vimeo.com/channels/staffpicks/93951774"  data-fancybox class="btn btn-primary text-white py-2 mr-3 text-uppercase letter-spacing-1">Plus...</a></p>
            </div>

        </div>
    </div>
</section>

<section class="section bg-image overlay" style="background-image: url('../content/assets/images/hero_4.jpg');">
    <div class="container" >
        <div class="row align-items-center">
            <div class="col-12 col-md-6 text-center mb-4 mb-md-0 text-md-left" data-aos="fade-up">
                <h2 class="text-white font-weight-bold">Simple, rapide et pas de frais jeune conducteur !</h2>
            </div>
            <div class="col-12 col-md-6 text-center text-md-right" data-aos="fade-up" data-aos-delay="200">
                <!-- Extra large modal -->
                <a href="#" class="btn btn-outline-white-primary py-3 text-white px-5" data-toggle="modal" data-target="#reservation-form">Réserver maintenant !</a>
            </div>
        </div>
    </div>
</section>

<!-- Formulaire de réservation A VOIR POUR UNE POTENTIEL AMELIORATION
<div class="modal fade " id="reservation-form" tabindex="-1" role="dialog" aria-labelledby="reservationFormTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">

            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12" data-aos="fade-up" data-aos-delay="100">

                        <form action="index.html"  method="post" class="bg-white p-4">
                            <div class="row mb-4"><div class="col-12"><h2>Reservation</h2></div></div>
                            <div class="row">
                                <div class="col-md-6 form-group">
                                    <label class="text-black font-weight-bold" for="name">Name</label>
                                    <input type="text" id="name" class="form-control ">
                                </div>
                                <div class="col-md-6 form-group">
                                    <label class="text-black font-weight-bold" for="phone">Phone</label>
                                    <input type="text" id="phone" class="form-control ">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12 form-group">
                                    <label class="text-black font-weight-bold" for="email">Email</label>
                                    <input type="email" id="email" class="form-control ">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6 form-group">
                                    <label class="text-black font-weight-bold" for="checkin_date">Date Check In</label>
                                    <input type="text" id="checkin_date" class="form-control">
                                </div>
                                <div class="col-md-6 form-group">
                                    <label class="text-black font-weight-bold" for="checkout_date">Date Check Out</label>
                                    <input type="text" id="checkout_date" class="form-control">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6 form-group">
                                    <label for="adults" class="font-weight-bold text-black">Adults</label>
                                    <div class="field-icon-wrap">
                                        <div class="icon"><span class="ion-ios-arrow-down"></span></div>
                                        <select name="" id="adults" class="form-control">
                                            <option value="">1</option>
                                            <option value="">2</option>
                                            <option value="">3</option>
                                            <option value="">4+</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 form-group">
                                    <label for="children" class="font-weight-bold text-black">Children</label>
                                    <div class="field-icon-wrap">
                                        <div class="icon"><span class="ion-ios-arrow-down"></span></div>
                                        <select name="" id="children" class="form-control">
                                            <option value="">1</option>
                                            <option value="">2</option>
                                            <option value="">3</option>
                                            <option value="">4+</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-4">
                                <div class="col-md-12 form-group">
                                    <label class="text-black font-weight-bold" for="message">Notes</label>
                                    <textarea name="message" id="message" class="form-control " cols="30" rows="8"></textarea>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 form-group">
                                    <input type="submit" value="Reserve Now" class="btn btn-primary text-white py-3 px-5 font-weight-bold">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
-->
<!-- Fin barre réservation -->
<?php

$contenu = ob_get_clean();
require "gabarit.php";