<?php
/**
 * Created by PhpStorm.
 * User: Bruno.DA-COSTA-LOURE
 * Date: 27.05.2019
 * Time: 09:20
 */
ob_start();
$titre="Véhicule - Modification";
?>
    <section class="site-hero overlay" style="background-image: url(../content/assets/images/hero_5.jpg)" data-stellar-background-ratio="0.5" id="section-home">
        <div class="container">
            <div class="row site-hero-inner justify-content-center align-items-center">
                <div class="col-md-12" data-aos="fade-up">
                    <div style="padding-top: 2%; padding-bottom: 4%;padding-right: 2%; width: 100%; margin-top: 10%;" class="box_title">
                        <div class="container">
                            <H1>Changement de mot de passe</H1>
                            <form method="POST" action="index.php?action=view_clientModifyData&uID=<?=$_GET['uID']?>" enctype="multipart/form-data">
                                <?php
                                //Vérification si le modèle nous renvoie un message d'erreur
                                switch($erreur)
                                {
                                    case 1:
                                        echo "<font STYLE='text-decoration:underline' color='red'>Le champs 'Boîte de vitesse' ne contient pas manuel ou automatique ! <br/><br/></font>";
                                        break;

                                    case 2:
                                        echo "<font STYLE='text-decoration:underline' color='red'>Le n° de matricule est invalide ! <br/><br/></font>";
                                        break;

                                    case 3:
                                        echo "<font STYLE='text-decoration:underline' color='red'>Le prix doit être un nombre ! <br/><br/></font>";
                                        break;

                                    case 4:
                                        echo "<font STYLE='text-decoration:underline' color='red'>Le nombre de portes est incohérent ! <br/><br/></font>";
                                        break;

                                    case 5:
                                        echo "<font STYLE='text-decoration:underline' color='red'>Certains champs sont vides ! <br/><br/></font>";
                                        break;

                                    default:
                                        echo"";
                                }
                                ?>
                                <div class="register-top-grid">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <span>Nouveau mot de passe<label>*</label></span>
                                            <input type="password" name="newPassword">
                                        </div>
                                        <div class="col-md-6">
                                            <span>Confirmation du mot de passe<label>*</label></span>
                                            <input type="password" name="newPassword2">
                                        </div>
                                        <div class="col-lg-5"></div>
                                        <div class="col-md-6 col-lg-2 align-self-end" style="padding-right: 40px; margin-top: 40px">
                                            <button class="btn btn-primary btn-block text-white" style="margin-left: auto; margin-right: auto; display block" type="submit" name="POST">Modifier</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php
$contenu = ob_get_clean();
require "gabarit.php";
