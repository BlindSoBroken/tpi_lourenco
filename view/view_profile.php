<?php
/**
 * Created by PhpStorm.
 * User: Bruno.DA-COSTA-LOURE
 * Date: 15.05.2019
 * Time: 16:39
 */
?>
<?php
/**
 * Created by PhpStorm.
 * User: Bruno.DA-COSTA-LOURE
 * Date: 14.05.2019
 * Time: 11:08
 */
ob_start();
$titre="Inscription";
?>
    <html>
    <body>
    <section class="site-hero overlay" style="background-image: url(../content/assets/images/hero_5.jpg)" data-stellar-background-ratio="0.5" id="section-home">
        <div class="container">
            <div class="row site-hero-inner justify-content-center align-items-center">
                <div class="col-md-12" data-aos="fade-up">
                    <div style="padding-top: 2%; padding-bottom: 4%;padding-right: 2%; width: 65%; margin:0 auto; margin-top: 15%; margin-bottom: 5%;" class="box_title">
                        <div class="container">
                            <form method="POST" action="index.php?action=view_profileData" enctype="multipart/form-data">
                                <div class="row">
                                    <?php
                                        //Vérification si le modèle nous renvoie un message d'erreur
                                        switch($erreur)
                                        {
                                            case 1:
                                                echo "<font STYLE='text-decoration:underline' color='red'>L'ancien mot de passe n'est pas le bon ! <br/><br/></font>";
                                                break;

                                            case 2:
                                                echo "<font STYLE='text-decoration:underline' color='red'>Les nouveaux mots de passes ne sont pas identiques ! <br/> <br/></font>";
                                                break;

                                            default:
                                                echo "";
                                        }
                                    ?>
                                    <div class="col-md-12">
                                        <H1><?php echo $userData['firstNameUser']." ".$userData['lastNameUser'];?></H1>
                                    </div>
                                    <div class="col-md-12"> <H2>Informations personnelles</H2></div>
                                    <div class="col-md-12" style="padding-top: 2%">
                                        Nom : <?php echo $userData['lastNameUser']; ?>
                                    </div>
                                    <div class="col-md-12">
                                        Prénom : <?php echo $userData['firstNameUser']; ?>
                                    </div>
                                    <div class="col-md-12">
                                        Adresse : <?php echo $userData['addressUser']; ?>
                                    </div>
                                    <div class="col-md-12">
                                        Code postal : <?php echo $userData['npaUser']; ?>
                                    </div>
                                    <div class="col-md-12">
                                        Ville : <?php echo $userData['cityUser']; ?>
                                    </div>
                                </div>
                                <div class="row" style="padding-top: 2%">
                                    <div class="col-md-12"><H2>Changement de mot de passe</H2></div>
                                    <div class="col-md-12" style="padding-top: 2%">
                                        <span>Ancien mot de passe</span><br/>
                                        <input type="password" name="profilOldPassword">
                                    </div>
                                    <div class="col-md-12" style="padding-top: 2%">
                                        <div class="wrap">
                                            <span style="height: 15px;">Nouveau mot de passe
                                                <img class="icon-secu" src="../content/assets/images/icone-info-secu.png">
                                                <div class="popup" style="top: -58px;left: 190px; padding-bottom: 140px">
                                                    <ul style="text-transform: uppercase;"><h6><u>Le mot de passe doit contenir au minimum :</u></h6>
                                                        <li style="color: #000;">Une majuscule</li>
                                                        <li style="color: #000;">Une minuscule</li>
                                                        <li style="color: #000;">Un chiffre</li>
                                                    </ul>
                                                </div>
                                            </span>
                                        </div>
                                        <input type="password" name="profilNewPassword">
                                    </div>
                                    <div class="col-md-12" style="padding-top: 2%">
                                        <span>Confirmation du nouveau mot de passe</span><br/>
                                        <input type="password" name="profilNewPassword2">
                                    </div>
                                    <div class="col-md-6 col-lg-9"></div>
                                    <div class="col-md-6 col-lg-3 align-self-end">
                                        <button class="btn btn-primary btn-block text-white" type="submit" name="POST">Modifier</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- END section -->
    </body>
    </html>
<?php
$contenu = ob_get_clean();
require "gabarit.php";

