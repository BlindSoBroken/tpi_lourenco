<?php
/**
 * Created by PhpStorm.
 * User: Bruno.DA-COSTA-LOURE
 * Date: 15.05.2019
 * Time: 14:00
 */

ob_start();
$titre="Déconnexion";
session_destroy();
Header("Location:index.php?action=view_home");

?>