<?php
/**
 * Created by PhpStorm.
 * User: Bruno.DA-COSTA-LOURE
 * Date: 13.02.2019
 * Time: 08:37
 */
?>
<?php
/*
 * User: Bruno.Lourenco / Franck.Bulliard
 * Date: 16.06.2017
 */

// Tampon de flux stocké en mémoire
ob_start();
$titre = "erreur";
?>

    <article>
        <header>
            <h2>Erreur</h2>
            <p>
                L'action demandée est inconnue !
            </p>
            <!-- Nous retourne l'erreur si la page est face à un problème technique -->
            <?=@$e;?>
        </header>
    </article>
<?php
$contenu = ob_get_clean();
require 'gabarit.php';