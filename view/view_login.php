<?php
/**
 * Created by PhpStorm.
 * User: Bruno.DA-COSTA-LOURE
 * Date: 15.05.2019
 * Time: 08:19
 */
ob_start();
$titre="Connexion";
?>
<html>
    <body>
    <section class="site-hero overlay" style="background-image: url(../content/assets/images/hero_5.jpg)" data-stellar-background-ratio="0.5" id="section-home">
        <div class="container">
            <div class="row site-hero-inner justify-content-center align-items-center">
                <div class="col-md-12" data-aos="fade-up">
                    <div style="padding-top: 2%; padding-bottom: 4%;padding-right: 2%; width: 65%; margin: 0 auto;" class="box_title">
                        <div class="container">
                            <H1>Connexion</H1>
                            <div class="col-md-6 col-lg-7 align-self-end">
                                <a href="index.php?action=view_register"><u>Je ne possède pas de compte</u></a>
                            </div>
                            <form method="POST" action="index.php?action=view_login_data" enctype="multipart/form-data">
                                <?php
                                //Vérification si le modèle nous renvoie un message d'erreur
                                if($erreur)
                                {
                                    echo "<font STYLE='text-decoration:underline' color='red'>Les identifiants sont incorrects ! <br/> <br/></font>";
                                }
                                ?>
                                <div class="register-top-grid">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <span>E-mail / Nom d'utilisateur<label>*</label></span>
                                            <input type="text" name="connexionIdentifiant">
                                        </div>
                                        <div class="col-md-12">
                                            <span>Mot de passe<label>*</label></span>
                                            <input type="password" name="connexionPassword">
                                        </div>
                                        <div class="col-md-6 col-lg-7 align-self-end">
                                            <a href="index.php?action=view_register"><u>Mot de passe oublié ?</u></a>
                                        </div>
                                        <div class="col-md-5 col-lg-4 align-self-end">
                                            <button class="btn btn-primary btn-block text-white" type="submit" name="POST">Connexion</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- END section -->
    </body>
</html>
<?php
$contenu = ob_get_clean();
require "gabarit.php";

