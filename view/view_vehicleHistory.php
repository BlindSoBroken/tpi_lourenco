<?php
/**
 * Created by PhpStorm.
 * User: Bruno.DA-COSTA-LOURE
 * Date: 28.05.2019
 * Time: 16:14
 */
ob_start();
$titre="Historique";

setlocale(LC_TIME, 'fr_FR.utf8','fra');
?>
<section class="site-hero overlay" style="background-image: url(../content/assets/images/hero_5.jpg)" data-stellar-background-ratio="0.5" id="section-home">
    <div class="container">
        <div class="row site-hero-inner justify-content-center align-items-center">
            <div class="col-md-12" data-aos="fade-up">
                <div style="padding-top: 2%; padding-bottom: 4%;padding-right: 38px;padding-left: 40px; width: 100%; margin: 0 auto;" class="box_title">
                    <div class="container">
                        <H1 style="padding-bottom: 20px">Historique de : <b><?=$history[0]['brand']. " " .$history[0]['model']?></b></H1>
                        <div class="row" style="padding-bottom: 2%;color: #FFFFFF; background-color: black;">
                            <div class="col-md-2 gestionAdmin">
                                <b>Nom</b>
                            </div>
                            <div class="col-md-2 gestionAdmin">
                                <b>Prénom</b>
                            </div>
                            <div class="col-md-2 gestionAdmin">
                                <b>Date de début</b>
                            </div>
                            <div class="col-md-2 gestionAdmin">
                                <b>Date de fin</b>
                            </div>
                            <div class="col-md-2 gestionAdmin">
                                <b>Prix total</b>
                            </div>
                            <div class="col-md-2 gestionAdmin">
                                <b>État du paiement</b>
                            </div>
                        </div>
                        <div class="row">
                            <?php foreach ($history as $info):?>
                                <div class="col-md-2 gestionAdmin" style="border-left: solid black 2px;">
                                    <?= $info['lastNameUser']?>
                                </div>
                                <div class="col-md-2 gestionAdmin">
                                    <?= $info['firstNameUser']?>
                                </div>
                                <div class="col-md-2 gestionAdmin">
                                    <?= strftime("%d %B %Y",strtotime($info['bookingDateStart']));?>
                                </div>
                                <div class="col-md-2 gestionAdmin">
                                    <?= strftime("%d %B %Y",strtotime($info['bookingDateEnd']));?>
                                </div>
                                <div class="col-md-2 gestionAdmin">
                                    <?php
                                        $diff=strtotime($info['bookingDateEnd']) - strtotime($info['bookingDateStart']);
                                        echo $info['locationPricePerDay'] * round($diff / (60*60*24));
                                    ?>.- CHF
                                </div>
                                <div class="col-md-2 gestionAdmin" style="border-right: solid black 2px;">
                                    <?php if($info['bookingPaiementState']==1){echo "Payé";}else{echo "Non payé";}?>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
$contenu = ob_get_clean();
require "gabarit.php";
