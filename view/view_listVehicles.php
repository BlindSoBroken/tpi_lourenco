<?php
/**
 * Created by PhpStorm.
 * User: Bruno.DA-COSTA-LOURE
 * Date: 23.05.2019
 * Time: 13:44
 */
$titre="Liste";
ob_start();
?>

<section class="site-hero overlay" data-stellar-background-ratio="0.5" id="section-home">
    <div class="container">
        <div class="row site-hero-inner justify-content-center align-items-center">
            <div class="col-md-12" data-aos="fade-up">
                <div style="padding-top: 2%; padding-bottom: 4%;padding-right: 2%; width: 100%; margin-top: 15%; margin-bottom: 5%;" class="box_title">
                    <div class="container">
                        <H1>Listes de nos véhicules</H1>
                            <?php
                            //Vérification si le modèle nous renvoie un message d'erreur
                            switch($erreur)
                            {
                                case 1:
                                    echo "<font STYLE='text-decoration:underline' color='red'>Certains champs sont vides ! <br/><br/></font>";
                                    break;

                                case 2:
                                    echo "<font STYLE='text-decoration:underline' color='red'>Le code postal est incorrect ! <br/> <br/></font>";
                                    break;

                                case 3:
                                    echo "<font STYLE='text-decoration:underline' color='red'>Le format de l'adresse mail est invalide ! <br/> <br/></font>";
                                    break;

                                case 4:
                                    echo "<font STYLE='text-decoration:underline' color='red'>Le mot de passe ne correspond pas aux critères de sécurité demandé ! <br/> <br/></font>";
                                    breaK;

                                case 5:
                                    echo "<font STYLE='text-decoration:underline' color='red'>Les mots de passes ne sont pas identiques ! <br/> <br/></font>";
                                    break;

                                case 6:
                                    echo "<font STYLE='text-decoration:underline' color='red'>Les informations personnelle ne sont pas valide ! <br/> <br/></font>";
                                    break;

                                case 7:
                                    echo "<font STYLE='text-decoration:underline' color='red'>Les informations de connexion ne sont pas valide ! <br/> <br/></font>";
                                    break;

                                default:
                                    echo "";
                            }
                            ?>
                            <div class="register-top-grid">
                                <div class="row">
                                    <?php
                                    //Vérification si il y a des véhicules disponibles
                                    if (empty($vehicles))
                                    {
                                   ?>   <div class="col-md-12">
                                            <h3 style="text-align: center;padding-top: 20px">Aucun véhicule n'est disponible dans les dates saisies</h3>
                                        </div>
                                        <div class="col-md-6 col-lg-2 align-self-end">
                                            <button class="btn btn-primary btn-block text-white"><a href="index.php?action=view_home">Retour</a></button>
                                        </div>
                                    <?php
                                    }
                                    else
                                    {
                                        foreach ($vehicles as $vehicleInfo):
                                            {
                                                ?>
                                                <div class="shop_box">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <a href="index.php?action=view_vehicle&vID='<?=$vehicleInfo['idVehicle']?>'">
                                                                <img style="border: 1px solid #E0E0E0;" src="<?=$vehicleInfo['picture']?>" width="308" height="244" class="img-responsive" alt=""/>
                                                            </a>
                                                        </div>
                                                        <div class="col-md-5">
                                                            <div class="shop_desc" style="margin: 0;">
                                                                <h2><b><?=$vehicleInfo['brand']." ".$vehicleInfo['model']?></b></h2>
                                                                <br/>
                                                                <p style="text-align: justify;text-justify: none">
                                                                    <b>Kilomètres au compteur :</b> <?=$vehicleInfo['mileage']?><br/>

                                                                    <b>Boîte de vitesse :</b> <?=$vehicleInfo['gearBox']?><br/>

                                                                    <b>Nombre de portes :</b><?=$vehicleInfo['doors']?><br/>

                                                                <h4>Prix de la location :
                                                                    <b><u><?=number_format($vehicleInfo['locationPricePerDay'],2,',',' ')?></u></b></h4>
                                                                </p>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <a href="index.php?action=view_vehicle&vID=<?=$vehicleInfo['idVehicle']?>"><button class="btn btn-primary btn-block text-white" style="margin-top:110px">Afficher</button></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php
                                            } endforeach;
                                    }?>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
$contenu = ob_get_clean();
require "gabarit.php";