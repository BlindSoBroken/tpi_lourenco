<?php
/**
 * Created by PhpStorm.
 * User: Bruno.DA-COSTA-LOURE
 * Date: 14.05.2019
 * Time: 09:21
 */

/***
 * @param $erreur
 * @param $registrationOK
 * This function is used to display the home page
 */
function home($erreur, $registrationOK)
{
    require "view/view_home.php";
}

/***
 * @param $error
 * This function is used to display the register page
 */
function register($error)
{
    require "view/view_register.php";
}

/***
 * @param $erreur
 * This function is used to display the login page
 */
function login($erreur)
{
    require "view/view_login.php";
}

/***
 * This function is used to display the logout page
 */
function logout()
{
    require "view/view_logout.php";
}

/***
 * This function is used to display the client management panel
 */
function clientsManagement()
{
    require_once "model/model.php";
    $gestion = getClients();
    require "view/view_clientsManagement.php";
}

/***
 * @param $erreur
 * This function is used to display the vehicle management panel
 */
function vehiclesManagement($erreur)
{
    require_once "model/model.php";
    $gestion = getVehicles();
    require "view/view_vehiclesManagement.php";
}

/***
 * @param $user
 * @param $erreur
 * @throws Exception
 * This function is used to display the profile page
 */
function profile($user, $erreur)
{
    require_once "model/model.php";
    $userData=getProfileData($user);
    require "view/view_profile.php";
}

/***
 * @param $erreur
 * This function is used to display the vehicle adding form
 */
function vehicleAddingForm($erreur)
{
    require "view/view_vehiclesAdd.php";
}

/***
 * This function is used to display the vehicle page
 */
function vehicle_consult()
{
    require_once "model/model.php";
    $vehicle=getVehiclesSingle($_GET['vID']);
    require "view/view_vehicle.php";
}

/***
 * @param $erreur
 * This function is used to display the vehicle modify form
 */
function vehicle_modify($erreur)
{
    require_once "model/model.php";
    $vehicleInfo=getVehiclesSingle($_GET['vID']);
    require "view/view_vehicleModify.php";
}

/***
 * @param $erreur
 * his function is used to display the client's password modify form
 */
function client_ModifyForm($erreur)
{
    require "view/view_clientModify.php";
}

/***
 * @param $data
 * This function is used to check if the data is valid and after the verifications display the client management panel
 */
function clientModifyData($data)
{
    try
    {
        require_once "model/model.php";
        updatePWD($data,$_GET['uID']);
        clientsManagement();
    }
    catch (exception $e)
    {
        client_ModifyForm($e->getMessage());
    }
}

/***
 * @param $data
 * @param $erreur
 * This function is used to display the list of vehicles page
 */
function listVehicles($data,$erreur)
{
    try
    {
        require_once "model/model.php";
        if (empty($data))
        {
            $vehicles=getVehicles();
            require "view/view_listVehicles.php";
        }
        else
        {
            $_SESSION['booking']=checkBooking($data);
            $diff=strtotime($_SESSION['booking']['dateEnd']) - strtotime($_SESSION['booking']['dateStart']);
            $_SESSION['nbDays']=round($diff / (60*60*24));
            $vehicles=getVehiclesAvailable($_SESSION['booking']);

            require "view/view_listVehicles.php";
        }
    }
    catch (exception $e)
    {
        home($e->getMessage(),"");
    }
}

/***
 * This function is used to verify the booking data and add it to the database
 */
function addBooking()
{
    require_once "model/model.php";
    $dataBooking=createArrayBook();
    addReservation($dataBooking);
    myBookings("");
}

/***
 * @param $erreur
 * This function is used to display the bookings of the user page
 */
function myBookings($erreur)
{
    require_once "model/model.php";
    $gestionBook=listBookings($_SESSION['uID']);

    //Tentative de faire en sorte de ne pas affiché les réservations expirée
    /*if ($gestionBook['0']['bookingDateEnd']==date("Y-m-d"))
    {
        expireBooking();
    }*/
    require "view/view_mybookings.php";
}

/***
 * @param $erreur
 * This function is used to check if the vehicle is available and if so then we can delete it and display the vehicle management panel
 */
function vehicle_delete($erreur)
{
    try
    {
        require_once "model/model.php";
        $vehicleInfo=getVehicleDisponibility($_GET['vID']);
        if ($vehicleInfo['disponibility'] == 1)
        {
            vehicleDelete($_GET['vID']);
            vehiclesManagement("");
        }
        elseif ($vehicleInfo['disponibility'] == 0)
        {
            $erreur = "true";
            throw new Exception($erreur);
        }
    }
    catch (exception $e)
    {
        vehiclesManagement($e->getMessage());
    }

}

/***
 * This function is used to delete a user and display the client management panel
 */
function client_delete()
{
      require_once "model/model.php";
      deleteClient($_GET['uID']);
      clientsManagement();
}

/***
 * @param $vehicleData
 * This function is used to check the data and then modify the informations in the database
 */
function vehicleModifyData($vehicleData)
{
    try
    {
        require_once "model/model.php";
        vehicleModify($_GET['vID'], $vehicleData);
        vehiclesManagement("");
    }
    catch (exception $e)
    {
        vehicle_modify($e->getMessage());
    }
}

/***
 * @param $vehicleData
 * This function is used to check the data and then add them in the database
 */
function vehicleAddData($vehicleData)
{
    try
    {
        require_once "model/model.php";
        vehicleAdd($vehicleData);
        vehiclesManagement("");
    }
    catch (exception $e)
    {
        vehicleAddingForm($e->getMessage());
    }
}

/***
 * This function is used to display the booking's history of a vehicle page
 */
function vehicleHistory()
{
    require_once "model/model.php";
    $history=getVehicleHistoryInfo($_GET['vID']);

    require "view/view_vehicleHistory.php";

}

/***
 * This function is used to display the booking's history of an user page
 */
function clientHistory()
{
    require_once "model/model.php";
    $history=getClientHistoryInfo($_GET['uID']);

    require "view/view_clientHistory.php";
}

/***
 * @param $user
 * @throws Exception
 * This function is used to change the password of an user and display the profile page
 */
function profileData($user)
{
    try
    {
        if (isset($_POST['profilOldPassword']))
        {
            require_once "model/model.php";
            updatePWD($_POST, $user);
        }
    }
    catch(exception $e)
    {
        profile("",$e->getMessage());
    }
}

/***
 * @param $registerValues
 * This function is used to check the informations before adding the user in the database and display the home page with an alert message
 */
function registerData($registerValues)
{
    try
    {
        require_once "model/model.php";
        $sessionData=sign_in($registerValues);
        home("",$registrationOK="true");
    }
    catch(exception $e)
    {
        register($e->getMessage());
    }
}

/***
 * @param $connexionValues
 *
 * This function is used to check if the user exists in the database before connecting him
 */
function login_data($connexionValues)
{
    try
    {
        if(isset($connexionValues))
        {
            require_once "model/model.php";
            $sessionData=log_in($connexionValues);
            if($sessionData!="")
            {
                createSession($sessionData);
            }
        }
        else
        {
            $erreur = "true";
            throw new Exception($erreur);
        }

    }
    catch (exception $e)
    {
        login($e->getMessage());
    }
}

/***
 * @param $sessionData
 * This function is used to create the Sessions variables with the user's data
 */
function createSession($sessionData)
{
    $_SESSION['mail']=$sessionData['mailAddressUser'];
    $_SESSION['login']=$sessionData['loginUser'];
    $_SESSION['uID']=$sessionData['idUser'];
    if (isset($sessionData['fkcategoryUser']))
    {
        $_SESSION['categoryUser']=$sessionData['fkcategoryUser'];
    }

    require "view/view_login-register_complete.php";
}

/***
 * @param $e
 * This function is used to get an error message to help me with the debugging process
 */
function erreur($e)
{
    require "view/view_error.php";
}