-- MySQL Script created by Bruno Da Costa Louren�o
-- 15.05.2019
-- MySQL WORKBENCH
-- Model: AutoSwissRent    Version: 1.0

USE `bdacosta_autoCH`;
-- -----------------------------------------------------
-- Table Categorie d'utilisateur
-- -----------------------------------------------------
CREATE TABLE `categoryUser` (
  `idCategoryUser` INT NOT NULL AUTO_INCREMENT,
  `categoryName` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idCategoryUser`)
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=`utf8_general_ci`;

INSERT INTO `categoryUser` (`idCategoryUser`, `categoryName`) VALUES
(0, 'Administrateur'),
(1, 'Client');

-- -----------------------------------------------------
-- Table Historique
-- -----------------------------------------------------
CREATE TABLE `history` (
  `idHistory` INT NOT NULL AUTO_INCREMENT,
  `totalLocationPrice` INT NULL,
  PRIMARY KEY (`idHistory`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=`utf8_general_ci`;

-- -----------------------------------------------------
-- Table Methode de paiement
-- -----------------------------------------------------
CREATE TABLE `payingMethods` (
  `idPayingMethods` INT NOT NULL AUTO_INCREMENT,
  `methodName` VARCHAR(45) NOT NULL,
  `methodType` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idpayingMethods`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=`utf8_general_ci`;

-- -----------------------------------------------------
-- Table Utilisateurs
-- -----------------------------------------------------
CREATE TABLE `users` (
  `idUser` INT NOT NULL AUTO_INCREMENT,
  `firstNameUser` VARCHAR(25) NOT NULL,
  `lastNameUser` VARCHAR(50) NOT NULL,
  `addressUser` VARCHAR(60) NOT NULL,
  `npaUser` INT(4) NOT NULL,
  `cityUser` VARCHAR(45) NOT NULL,
  `mailAddressUser` VARCHAR(100) NOT NULL,
  `loginUser` VARCHAR(45) NOT NULL,
  `passwordUser` TEXT NOT NULL,
  `fkhistory` INT NULL,
  `fkcategoryUser` INT DEFAULT 1,
  `fkpayingMethods` INT NULL,
  PRIMARY KEY (`idUser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=`utf8_general_ci`;

INSERT INTO `users`(`firstNameUser`, `lastNameUser`, `addressUser`, `npaUser`, `cityUser`, `mailAddressUser`, `loginUser`, `passwordUser`, fkcategoryUser) VALUES
                   ("Franck", "Bulliard", "Chemin de la brume 4", 1823,"La Tour-de-peilz", "franck-15@hotmail.fr", "Franck15", "$2y$10$KWS5hacqI5CyAte6ZqG7QeP6h.R9PsjBeuVZTWXlTs1BYnjmaqpRK", 0),
				   ("Theo", "Mauron", "Chemin des oliviers", 1358,"Valeyres-sous-rances", "theo.mauron@cpnv.ch", "TRAK3N", "$2y$10$PraTXZGCC7Vbnlq1UgmQquJvZEID/vYgitevRnqcuFGzVRroggpTq", 1);

-- -----------------------------------------------------
-- Table V�hicule
-- -----------------------------------------------------
CREATE TABLE `vehicle` (
  `idVehicle` INT NOT NULL AUTO_INCREMENT,
  `brand` VARCHAR(45) NULL,
  `model` VARCHAR(45) NULL,
  `registrationNumber` VARCHAR(30) NOT NULL,
  `locationPricePerDay` SMALLINT NOT NULL,
  `mileage` INT NOT NULL,
  `gearBox` VARCHAR(15) NOT NULL,
  `doors` TINYINT NOT NULL,
  `picture` TEXT NULL,
  `fkusers` INT NULL,
  `fkhistory` INT NOT NULL,
  PRIMARY KEY (`idVehicle`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=`utf8_general_ci`;


-- -----------------------------------------------------
-- Table R�servation
-- -----------------------------------------------------
CREATE TABLE `booking` (
  `idBooking` INT NOT NULL AUTO_INCREMENT,
  `bookingDate` DATE NOT NULL,
  `bookingLength` VARCHAR(45),
  `bookingPrice` INT,
  `disponibility` TINYINT,
  `fkusers` INT NOT NULL,
  `fkvehicle` INT NOT NULL,
  PRIMARY KEY (`idBooking`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=`utf8_general_ci`;

--
-- Contrainte de tables
--

ALTER TABLE `users`
  ADD CONSTRAINT `fkusers_history_idHistory` FOREIGN KEY (fkhistory) REFERENCES `history` (`idHistory`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fkusers_categoryUser_idCategoryUser` FOREIGN KEY (fkcategoryUser) REFERENCES `categoryUser` (`idCategoryUser`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fkusers_payingMethods_idPayingMethods` FOREIGN KEY (fkpayingMethods) REFERENCES `payingMethods` (`idPayingMethods`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `vehicle`
  ADD CONSTRAINT `fkvehicle_users_idUser` FOREIGN KEY (fkusers) REFERENCES `users` (`idUser`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fkvehicle_history_idHistory` FOREIGN KEY (fkhistory) REFERENCES `history` (`idHistory`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `booking`
  ADD CONSTRAINT `fkbooking_users_idUser` FOREIGN KEY (fkusers) REFERENCES `users` (`idUser`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fkbooking_vehicle_idVehicle` FOREIGN KEY (fkvehicle) REFERENCES `vehicle` (`idVehicle`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `users`
	ADD UNIQUE INDEX `mailAddressUser` (`mailAddressUser`);


