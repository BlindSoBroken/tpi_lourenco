-- MySQL Script created by Bruno Da Costa Louren�o
-- 15.05.2019
-- MySQL WORKBENCH
-- Model: AutoSwissRent    Version: 1.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET @@global.sql_mode= '';
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

-- -----------------------------------------------------
-- Schema ProjectD
-- -----------------------------------------------------
/*DROP DATABASE `projectd`;*/
CREATE SCHEMA IF NOT EXISTS `autoswissrent` DEFAULT CHARACTER SET `utf8` DEFAULT COLLATE `utf8_general_ci`;
 ;
USE `autoswissrent`;
-- -----------------------------------------------------
-- Table Categorie d'utilisateur
-- -----------------------------------------------------
CREATE TABLE `categoryUser` (
  `idCategoryUser` INT NOT NULL AUTO_INCREMENT,
  `categoryName` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idCategoryUser`)
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=`utf8_general_ci`;

INSERT INTO `categoryUser` (`idCategoryUser`, `categoryName`) VALUES
(0, 'Administrateur'),
(1, 'Client');

-- -----------------------------------------------------
-- Table Methode de paiement
-- -----------------------------------------------------
CREATE TABLE `payingMethods` (
  `idPayingMethods` INT NOT NULL AUTO_INCREMENT,
  `methodName` VARCHAR(45) NOT NULL,
  `methodType` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idpayingMethods`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=`utf8_general_ci`;

-- -----------------------------------------------------
-- Table Utilisateurs
-- -----------------------------------------------------
CREATE TABLE `users` (
  `idUser` INT NOT NULL AUTO_INCREMENT,
  `firstNameUser` VARCHAR(25) NOT NULL,
  `lastNameUser` VARCHAR(50) NOT NULL,
  `addressUser` VARCHAR(60) NOT NULL,
  `npaUser` INT(4) NOT NULL,
  `cityUser` VARCHAR(45) NOT NULL,
  `mailAddressUser` VARCHAR(100) NOT NULL,
  `loginUser` VARCHAR(45) NOT NULL,
  `passwordUser` TEXT NOT NULL,
  `fkcategoryUser` INT DEFAULT 1,
  `fkpayingMethods` INT NULL,
  PRIMARY KEY (`idUser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=`utf8_general_ci`;

INSERT INTO `users`(`firstNameUser`, `lastNameUser`, `addressUser`, `npaUser`, `cityUser`, `mailAddressUser`, `loginUser`, `passwordUser`, fkcategoryUser) VALUES
                   ("Franck", "Bulliard", "Chemin de la brume 4", 1823,"La Tour-de-peilz", "franck-15@hotmail.fr", "Franck15", "$2y$10$KWS5hacqI5CyAte6ZqG7QeP6h.R9PsjBeuVZTWXlTs1BYnjmaqpRK", 0),
				   ("Theo", "Mauron", "Chemin des oliviers", 1358,"Valeyres-sous-rances", "theo.mauron@cpnv.ch", "TRAK3N", "$2y$10$PraTXZGCC7Vbnlq1UgmQquJvZEID/vYgitevRnqcuFGzVRroggpTq", 1);

-- -----------------------------------------------------
-- Table V�hicules
-- -----------------------------------------------------
CREATE TABLE `vehicles` (
  `idVehicle` INT NOT NULL AUTO_INCREMENT,
  `brand` VARCHAR(45) NULL,
  `model` VARCHAR(45) NULL,
  `registrationNumber` VARCHAR(30) NOT NULL,
  `locationPricePerDay` SMALLINT NOT NULL,
  `mileage` INT NOT NULL,
  `gearBox` VARCHAR(15) NOT NULL,
  `doors` TINYINT NOT NULL,
  `picture` TEXT NOT NULL,
  `disponibility` TINYINT NOT NULL,
  PRIMARY KEY (`idVehicle`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=`utf8_general_ci`;

INSERT INTO `vehicles`(`brand`, `model`, `registrationNumber`, `locationPricePerDay`, `mileage`, `gearBox`, `doors`, `picture`, `disponibility`)VALUES 
("Audi", "R9", 601999, 19.95, 10000, "automatique", 5, "content/data/vehicles/Audi-R8-601999/Audi-R8-601999.jpg", 1);

-- -----------------------------------------------------
-- Table R�servation
-- -----------------------------------------------------
CREATE TABLE `booking` (
  `idBooking` INT NOT NULL AUTO_INCREMENT,
  `bookingDateStart` DATE NOT NULL,
  `bookingHourWithdrawal` TIME NOT NULL,
  `bookingDateEnd` DATE NOT NULL,
  `bookingHourDelivery` TIME NOT NULL,
  `bookingPaiementState` INT NOT NULL,
  `fkusers` INT NOT NULL,
  `fkvehicles` INT NOT NULL,
  PRIMARY KEY (`idBooking`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=`utf8_general_ci`;

INSERT INTO booking (bookingDateStart, bookingHourWithdrawal, bookingDateEnd, bookingHourDelivery, bookingPaiementState, fkusers, fkvehicles) VALUES
('019-05-26','11:00:00','2019-05-27','14:00:00',1,2,2);

--
-- Contrainte de tables
--

ALTER TABLE `users`
  ADD CONSTRAINT `fkusers_categoryUser_idCategoryUser` FOREIGN KEY (fkcategoryUser) REFERENCES `categoryUser` (`idCategoryUser`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fkusers_payingMethods_idPayingMethods` FOREIGN KEY (fkpayingMethods) REFERENCES `payingMethods` (`idPayingMethods`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `booking`
  ADD CONSTRAINT `fkbooking_users_idUser` FOREIGN KEY (fkusers) REFERENCES `users` (`idUser`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fkbooking_vehicles_idvehicle` FOREIGN KEY (fkvehicles) REFERENCES `vehicles` (`idvehicle`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `users`
	ADD UNIQUE INDEX `mailAddressUser` (`mailAddressUser`);

--
-- Cr�ation d'un utilisateur afin de g�rer la DB
--
CREATE USER IF NOT EXISTS 'bcl'@'%'
IDENTIFIED BY '#$porting007';
GRANT SELECT,INSERT,UPDATE,DELETE,CREATE,DROP,ALTER,EXECUTE ON autoswissrent.* TO 'bcl'@'%';

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

